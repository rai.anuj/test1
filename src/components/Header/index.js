import React from "react";
import { Link } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  Dropdown,
  Container,
  InputGroup,
  Form,
  NavDropdown,
  Button,
} from "react-bootstrap";
import { withRouter } from "react-router";
import { defaultAvatar } from "../../utils/images";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { toggleSidebar } from "../../store/actions/sidebar";
import { doLogout } from "../../store/actions/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import appLogo from "../../assets/images/app-logo.png";
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dropdownOpen: false,
      color: "transparent",
      resize: false,
    };
    this.toggle = this.toggle.bind(this);
    this.dropdownToggle = this.dropdownToggle.bind(this);
  }
  componentDidMount() {
    setInterval(() => {}, 1000);
  }

  toggle() {
    if (this.state.isOpen) {
      this.setState({
        color: "transparent",
      });
    } else {
      this.setState({
        color: "dark",
      });
    }
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  dropdownToggle(e) {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }
  getBrand() {
    let brandName = "iSabha";

    return brandName;
  }
  toggleSidebar() {
    if (window.innerWidth > 992) {
      document.body.classList.toggle("sidebar-mini");
    } else {
      document.body.classList.toggle("nav-open");
    }
    this.setState({ isOpen: !this.state.isOpen });
    this.props.toggleSidebar();
  }

  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.body.className.indexOf("nav-open") !== -1
    ) {
      document.body.classList.toggle("nav-open");
    }
  }

  render() {
    return (
      <Navbar
        color="transparent"
        expand="lg"
        className={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "navbar-absolute fixed-top"
            : "navbar-absolute fixed-top " + "navbar-transparent "
        }
      >
        <Container fluid>
          <div className="navbar-wrapper">
            {document.body.classList.contains("sidebar-mini") && (
              <Navbar.Brand href="/" onClick={(e) => e.preventDefault()}>
                <img
                  alt=""
                  src={appLogo}
                  width="24"
                  height="24"
                  className="d-inline-block align-top mr-2"
                />
                iSabha
              </Navbar.Brand>
            )}
          </div>

          <Navbar className="justify-content-end nav-header-right">
            <form className="d-none d-md-block">
              <InputGroup className="no-border">
                <Form.Control placeholder="Search..." />
                <InputGroup.Append>
                  <InputGroup.Text>
                    <i className="nc-icon nc-zoom-split" />
                  </InputGroup.Text>
                </InputGroup.Append>
              </InputGroup>
            </form>
            <Nav activeKey="1">
              <Nav.Link>
                <i className="nc-icon nc-cart-simple" />
                <span
                  className="badge badge-danger badge-pill"
                  style={{ position: "absolute" }}
                >
                  {this.props.purchase.cartCount > 0
                    ? this.props.purchase.cartCount
                    : ""}
                </span>
              </Nav.Link>

              <NavDropdown
                title={
                  <img
                    src={this.props.photo || defaultAvatar()}
                    className="header-profile-img"
                  />
                }
                id="nav-dropdown-settings"
                className=" btn-rotate"
              >
                <NavDropdown.Item eventKey="4.1" to="/profile" as={Link}>
                  <span className="d-inline-block" style={{ width: "24px" }}>
                    <FontAwesomeIcon
                      icon={["fas", "user"]}
                      style={{ fontSize: "90%" }}
                      className="mr-2"
                    />
                  </span>
                  Profile
                </NavDropdown.Item>
                <NavDropdown.Item eventKey="4.1" to="/admin" as={Link}>
                  <span className="d-inline-block" style={{ width: "24px" }}>
                    <FontAwesomeIcon
                      icon={["fas", "cogs"]}
                      style={{ fontSize: "90%" }}
                      className="mr-2"
                    />
                  </span>
                  Admin
                </NavDropdown.Item>

                <NavDropdown.Divider />
                <NavDropdown.Item
                  eventKey="4.2"
                  onClick={() => {
                    this.props.doLogout();
                  }}
                >
                  <span className="d-inline-block" style={{ width: "24px" }}>
                    <FontAwesomeIcon
                      icon={["fas", "sign-out-alt"]}
                      style={{ fontSize: "90%" }}
                      className="mr-2"
                    />
                  </span>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar>
        </Container>
      </Navbar>
    );
  }
}

Header.propTypes = {
  toggleSidebar: PropTypes.func.isRequired,
  doLogout: PropTypes.func.isRequired,
  sidebar: PropTypes.bool.isRequired,
  purchase: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  sidebar: state.sidebar.toggled,
  purchase: state.purchase,
  photo: state.user.photo,
});

export default withRouter(
  connect(mapStateToProps, { toggleSidebar, doLogout })(Header)
);
