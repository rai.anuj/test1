import React from "react";

export default () => {
  return (
    <div className="loader-container">
      <div className="loader-wrapper">
        <div className="loader">Loading...</div>
      </div>
    </div>
  );
};
