import React from "react";
import { NavLink, Link } from "react-router-dom";
import { Nav } from "react-bootstrap";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import { withRouter } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toggleSidebar } from "../../store/actions/sidebar";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import appLogo from "../../assets/images/app-logo.png";

var ps;

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
    this.sidebar = React.createRef();
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    const path = this.props.location.pathname;
    if (
      routeName == "/" &&
      (path == "/home" || path == "/" || path.indexOf("/admin") == 0)
    ) {
      return "active";
    }
    return routeName != "/" && path.indexOf(routeName) == 0 ? "active" : "";
  }
  handleResize = () => {
    if (
      window.innerWidth <= 992 &&
      !document.body.classList.contains("sidebar-mini")
    ) {
      document.body.classList.add("sidebar-mini");
    }
  };
  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.sidebar.current, {
        suppressScrollX: true,
        suppressScrollY: false,
      });
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  toggleSidebar = () => {
    if (window.innerWidth > 992) {
      document.body.classList.toggle("sidebar-mini");
    } else {
      document.body.classList.toggle("nav-open");
    }
    setTimeout(this.props.toggleSidebar, 300);
  };

  render() {
    return (
      <div
        className="sidebar"
        data-color={this.props.bgColor}
        data-active-color={this.props.activeColor}
      >
        <div className="logo  d-none  d-lg-block">
          <a
            href="#"
            className="simple-text logo-mini"
            onClick={this.toggleSidebar}
          >
            <div className="logo-img">
              <FontAwesomeIcon icon={["fas", "bars"]} />
            </div>
          </a>
          <Link
            to="/"
            className="simple-text logo-normal"
            style={{ fontSize: "20px" }}
          >
            <img
              alt=""
              src={appLogo}
              width="30"
              height="30"
              className="d-inline-block align-top mr-2"
            />
            iSabha
          </Link>
        </div>
        <div className="sidebar-wrapper" ref={this.sidebar}>
          <Nav>
            {this.props.routes
              .filter((prop) => prop.sidebar)
              .map((prop, key) => {
                return (
                  <li
                    className={
                      this.activeRoute(prop.path) +
                      (prop.pro ? " active-pro" : "")
                    }
                    key={key}
                  >
                    <NavLink
                      onClick={() => {
                        if (window.innerWidth <= 992) {
                          setTimeout(this.props.toggleSidebar, 300);
                        }
                      }}
                      to={prop.base + prop.path}
                      className="nav-link"
                      activeClassName="active"
                    >
                      <i>
                        <FontAwesomeIcon icon={prop.icon} />
                      </i>
                      <p>{prop.name}</p>
                    </NavLink>
                  </li>
                );
              })}
          </Nav>
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  toggleSidebar: PropTypes.func.isRequired,
  sidebar: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  sidebar: state.sidebar.toggled,
});

export default withRouter(connect(mapStateToProps, { toggleSidebar })(Sidebar));
