import React, { useState } from "react";
import { Card, Col, Figure, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";

import defaultImg from "../../assets/images/app-logo.png";
import { getChannelImage } from "../../utils/images";

const ImgLogoContainer = styled.div`
  height: 300px;

  &:before {
    content: "";
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  img {
    max-height: 300px;
  }
`;
const Box = (props) => {
  let v = props.video;
  let image = getChannelImage(v.collectionvideoimages, "cover");
  return (
    <Card style={{ height: "100%" }}>
      <Card.Body>
        <Figure>
          <ImgLogoContainer>
            <Figure.Image
              className="p-4"
              alt={v.videotitle}
              src={image ? image : defaultImg}
            />
          </ImgLogoContainer>
          <hr />
          <div>
            <div
              className="d-inline-block text-left"
              style={{ width: props.showCart ? "calc(100% - 70px)" : "100%" }}
            >
              <Figure.Caption style={{ color: "#000" }}>
                {v.videotitle}
              </Figure.Caption>
            </div>
            {props.showCart && (
              <div
                className="d-inline-block text-right"
                style={{ width: "70px", verticalAlign: "top" }}
              >
                <Button
                  size="sm"
                  variant="danger"
                  onClick={props.addToCart}
                  className="mt-1"
                >
                  <i className="fa fa-shopping-cart" />
                </Button>
              </div>
            )}
          </div>
          <div className="text-left text-default" style={{ fontSize: "12px" }}>
            {v.videodescr}
          </div>
        </Figure>
      </Card.Body>
    </Card>
  );
};

export default Box;
