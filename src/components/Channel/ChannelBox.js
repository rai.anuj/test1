import React, { useState } from "react";
import { Card, Col, Figure } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";

import defaultImg from "../../assets/images/app-logo.png";
const ImgLogoContainer = styled.div`
  height: 300px;

  &:before {
    content: "";
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  img {
    max-height: 300px;
  }
`;
const Box = (props) => {
  return (
    <Col xs={12} sm={6} md={4} lg={3} className="pb-4">
      <Link to={props.url}>
        <Card>
          <Card.Body>
            <Figure>
              <ImgLogoContainer>
                <Figure.Image
                  className="p-4"
                  alt="Example Channel"
                  src={props.logo ? props.logo : defaultImg}
                />
              </ImgLogoContainer>

              <Figure.Caption>
                <Card.Title className="text-center" as="h6">
                  {props.title}
                </Card.Title>
              </Figure.Caption>
            </Figure>
          </Card.Body>
        </Card>
      </Link>
    </Col>
  );
};

export default Box;
