import React from "react";

import { Button } from "react-bootstrap";

class SimpleUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
    };
    this.fileInput = React.createRef();
  }

  handleFileChange(e) {
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
      });
    };
    reader.readAsDataURL(file);
  }

  handleSubmit = (e) => {
    e.preventDefault();
  };

  handleClick = () => {
    this.fileInput.current.click();
  };

  render() {
    return (
      <div className={`fileinput text-center ${this.props.className}`}>
        <input
          type="file"
          onChange={this.handleFileChange}
          ref={this.fileInput}
        />
        <Button
          className="btn-round"
          variant={this.props.btnclassName ? this.props.btnclassName : "info"}
          onClick={this.handleClick}
        >
          <i class="nc-icon nc-cloud-upload-94 mr-2" />
          {this.props.label}
        </Button>
      </div>
    );
  }
}

export default SimpleUpload;
