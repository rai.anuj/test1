import React, { Component } from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";

import { Container } from "react-bootstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import NotificationAlert from "../NotificationAlert";

import routes from "../../routes";
import Sidebar from "../Sidebar";
import Header from "../Header";
import Login from "../../routes/Auth/Login";
import Loader from "../Loader";
import { checkLogin } from "../../store/actions/auth";
import {
  keepAlive,
  getCountries,
  getCurrencies,
} from "../../store/actions/misc";
import PrivateRoute from "../Auth/PrivateRoute";
import ScrollToTop from "./ScrollToTop";
import "./App.scss";

class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getCountries();
    this.props.getCurrencies();
    this.props.checkLogin();
    setInterval(this.props.keepAlive, 60000);
  }
  componentDidUpdate() {}
  render() {
    if (this.props.isAuth === undefined) {
      return <Loader />;
    }
    return (
      <Router>
        <ScrollToTop>
          <div id="outer-container" style={{ height: "100%" }}>
            {this.props.isAuth && (
              <Sidebar routes={routes} bgColor="white" activeColor="danger" />
            )}

            <main
              id="page-wrap"
              className={this.props.isAuth ? "main-panel" : ""}
            >
              {this.props.isAuth && <Header />}
              <Container style={{ height: "100%", paddingTop: "65px" }} fluid>
                <div>{this.generateRoutes(routes)}</div>
              </Container>
            </main>
            <NotificationAlert />
          </div>
          {this.props.showLoader && <Loader />}
        </ScrollToTop>
      </Router>
    );
  }

  generateRoutes = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        if (route.isPublic) {
          return (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          );
        } else {
          return (
            <PrivateRoute
              authed={this.props.isAuth}
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          );
        }
      });
    }
    return <Switch>{result}</Switch>;
  };
}

App.propTypes = {
  checkLogin: PropTypes.func.isRequired,
  keepAlive: PropTypes.func.isRequired,
  notification: PropTypes.object.isRequired,
  showLoader: PropTypes.bool.isRequired,
  getCountries: PropTypes.func.isRequired,
  getCurrencies: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  notification: state.misc.notification,
  showLoader: state.misc.showLoader,
});

export default connect(mapStateToProps, {
  checkLogin,
  keepAlive,
  getCountries,
  getCurrencies,
})(App);
