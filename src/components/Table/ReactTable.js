/*eslint-disable*/
import React, { useEffect, useCallback, useState } from "react";
import {
  useTable,
  useFilters,
  useAsyncDebounce,
  useSortBy,
  usePagination,
  useGlobalFilter,
} from "react-table";
import classnames from "classnames";
// A great library for fuzzy filtering/sorting items
import { matchSorter } from "match-sorter";
// react plugin used to create DropdownMenu for selecting items
import Select from "react-select";

// reactstrap components
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import Pagination from "../Pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [(row) => row.values[id]] });
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = (val) => !val;

// Our table component
function Table({ columns, data, hasGlobalFilter }) {
  const [numberOfRows, setNumberOfRows] = React.useState({
    value: 10,
    label: "10 rows",
  });
  const [pageSelect, handlePageSelect] = React.useState({
    value: 0,
    label: "Page 1",
  });

  //const [globalSearchKeyword, setGlobalSearchKeyword] = useState("");
  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {
        return rows.filter((row) => {
          const rowValue = row.values[id];
          return rowValue !== undefined
            ? String(rowValue)
                .toLowerCase()
                .startsWith(String(filterValue).toLowerCase())
            : true;
        });
      },
    }),
    []
  );

  const renderFilter = (column) => {
    if (column.filterConfig) {
      if (column.filterConfig.type && column.filterConfig.type == "select") {
        return (
          <Form.Control
            as="select"
            onChange={(e) => {
              column.setFilter(e.target.value || undefined);
            }}
            defaultValue=""
          >
            <option disabled hidden value="" className="font-weight-bold">
              {column.filterConfig && column.filterConfig.placeholder
                ? column.filterConfig.placeholder
                : "Select " + column.render("Header")}
            </option>
            <option key="" value="">
              All
            </option>
            {column.filterConfig.options.map((o) => (
              <option key={o} value={o}>
                {o}
              </option>
            ))}
          </Form.Control>
        );
      }
    }
    return (
      <Form.Control
        type="text"
        onChange={(e) => column.setFilter(e.target.value)}
        placeholder={
          column.filterConfig && column.filterConfig.placeholder
            ? column.filterConfig.placeholder
            : "Search " + column.render("Header")
        }
      ></Form.Control>
    );
  };
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    visibleColumns,
    nextPage,
    pageOptions,
    pageCount,
    previousPage,
    canPreviousPage,
    canNextPage,
    setPageSize,
    gotoPage,
    setGlobalFilter,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageSize: 10, pageIndex: 0 },
      manualFilters: false,
      filterTypes,
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  // We don't want to render all of the rows for this example, so cap
  // it for this use case
  // const firstPageRows = rows.slice(0, 10);

  return (
    <>
      <div className="ReactTable -striped -highlight primary-pagination">
        {/*hasGlobalFilter && (
          <Row>
            <Col sm="12" md="8" lg="9"></Col>
            <Col sm="12" md="4" lg="3" className="pb-2">
              <Form.Control
                type="text"
                className="d-inline-block"
                onChange={(e) => {
                  setGlobalFilter(e.target.value);
                }}
                placeholder="Search"
              />
            </Col>
          </Row>
              )*/}
        <table {...getTableProps()} className="rt-table">
          <thead className="rt-thead -header">
            {headerGroups.map((headerGroup,index) => (
              <React.Fragment key={index}>
                <tr
                  {...headerGroup.getHeaderGroupProps()}
                  className="rt-tr text-white bg-default"
                  key="headerGroup0"
                >
                  {headerGroup.headers.map((column, key) => (
                    <th
                      key={`headerGroup0-${column.render("Header")}`}
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                      className={classnames("rt-th rt-resizable-header", {
                        "-cursor-pointer":
                          headerGroup.headers.length - 1 !== key,
                        "-sort-asc": column.isSorted && !column.isSortedDesc,
                        "-sort-desc": column.isSorted && column.isSortedDesc,
                      })}
                      style={{
                        width: column.actionColumn
                          ? column.width + "px"
                          : "auto",
                      }}
                    >
                      <div className="rt-resizable-header-content">
                        {column.render("Header")}
                      </div>
                      {/* Render the columns filter UI */}
                    </th>
                  ))}
                </tr>
                <tr
                  {...headerGroup.getHeaderGroupProps()}
                  className="rt-tr"
                  key="headerGroup1"
                >
                  {headerGroup.headers.map((column, key) => (
                    <th
                      key={`headerGroup1-${column.render("Header")}`}
                      style={{
                        width: column.actionColumn
                          ? column.width + "px"
                          : "auto",
                      }}
                      className="p-2 rt-td"
                    >
                      {column.filterable && renderFilter(column)}
                    </th>
                  ))}
                </tr>
              </React.Fragment>
            ))}
          </thead>
          <tbody {...getTableBodyProps()} className="rt-tbody">
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr
                  {...row.getRowProps()}
                  className={classnames(
                    "rt-tr",
                    { " -odd": i % 2 === 0 },
                    { " -even": i % 2 === 1 }
                  )}
                >
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="rt-td">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="clearfix pt-2">
          <div className="pagination-bottom">
            <Pagination
              gotoPage={gotoPage}
              previousPage={previousPage}
              nextPage={nextPage}
              pageCount={pageOptions.length}
              canPreviousPage={canPreviousPage}
              canNextPage={canNextPage}
              pageIndex={pageIndex}
              pageSize={pageSize}
              totalItems={data.length}
              setPageSize={setPageSize}
            />
          </div>
        </div>
      </div>
    </>
  );
}

// Define a custom filter filter function!
function filterGreaterThan(rows, id, filterValue) {
  return rows.filter((row) => {
    const rowValue = row.values[id];
    return rowValue >= filterValue;
  });
}

// This is an autoRemove method on the filter function that
// when given the new filter value and returns true, the filter
// will be automatically removed. Normally this is just an undefined
// check, but here, we want to remove the filter if it's not a number
filterGreaterThan.autoRemove = (val) => typeof val !== "number";

export default Table;
