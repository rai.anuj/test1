import { faOldRepublic } from "@fortawesome/free-brands-svg-icons";
import React from "react";

import { Form, InputGroup, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export const inputGroupPrepend = ({
  name,
  type,
  placeHolder,
  validation,
  defaultError,
  icon,
  errors,
  register,
  label_props,
  label,
}) => {
  return (
    <div>
      <div>
        <Form.Label {...label_props}>{label}</Form.Label>
      </div>
      <div>
        <InputGroup className={errors[name] && "has-danger"}>
          <InputGroup.Prepend>
            <InputGroup.Text>
              {Array.isArray(icon) ? (
                <FontAwesomeIcon icon={icon} />
              ) : (
                <i className={icon} />
              )}
            </InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            placeholder={placeHolder}
            name={name}
            ref={register(validation)}
            type={type}
          />
          {errors[name] && (
            <label className="error">
              {defaultError ? defaultError : errors[name].message}
            </label>
          )}
        </InputGroup>
      </div>
    </div>
  );
};

export const inputComponent = ({
  name,
  type,
  placeHolder,
  validation,
  defaultError,
  errors,
  register,
  label,
  col,
  defaultValue,
  selectOptions,
  rows,
  inputProps,
}) => {
  const group_props = col ? { as: Row } : {};
  const label_props = col ? { column: true, ...col[0] } : {};
  const frm_control = selectOptions ? (
    <Form.Control
      placeholder={placeHolder}
      name={name}
      ref={register(validation)}
      defaultValue={defaultValue}
      as="select"
      {...inputProps}
    >
      {selectOptions.map((o) => (
        <option key={o.value} value={o.value}>
          {o.label}
        </option>
      ))}
    </Form.Control>
  ) : (
    <Form.Control
      placeholder={placeHolder}
      name={name}
      ref={register(validation)}
      defaultValue={defaultValue}
      {...inputProps}
      {...(type == "textarea"
        ? { rows: rows || 4, as: "textarea" }
        : {
            type: type,
          })}
    />
  );
  const d_control = col ? <Col {...col[1]}>{frm_control}</Col> : frm_control;
  return (
    <Form.Group {...group_props} className={errors[name] && "has-danger"}>
      <Form.Label {...label_props}>{label}</Form.Label>
      {d_control}
      {errors[name] && (
        <label className="error">
          {defaultError ? defaultError : errors[name].message}
        </label>
      )}
    </Form.Group>
  );
};

export const checkBox = ({
  name,
  type,
  label,
  validation,
  errors,
  register,
  defaultError,
}) => {
  return (
    <Form.Group className={"form-check " + (errors[name] ? "has-danger" : "")}>
      <label className="form-check-label">
        <input
          type={type}
          className="form-check-input"
          name={name}
          ref={register(validation)}
        />
        <span className="form-check-sign"></span>
        {label}
      </label>
      {errors[name] && (
        <label className="error">
          {defaultError ? defaultError : errors[name].message}
        </label>
      )}
    </Form.Group>
  );
};
