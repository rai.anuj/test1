import React, { useState, useCallback, useRef, useEffect } from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { setNotification, setLoader } from "../../store/actions/misc";
import { Button } from "react-bootstrap";

import defaultImage from "../../assets/images/image_placeholder.jpg";
import { defaultAvatar } from "../../utils/images";

import CropModal from "./CropModal";

const ImageApp = ({
  setNotification,
  minWidth,
  minHeight,
  aspect,
  thumbnailClass,
  onCrop,
  initialImage,
  onRemove,
  setLoader,
}) => {
  const [cropShow, setCropShow] = useState(false);
  const [upImg, setUpImg] = useState(null);
  const [croppedImage, setCroppedImage] = useState(null);
  const inputEl = useRef(null);
  useEffect(() => {
    setCroppedImage(initialImage);
  }, [initialImage]);
  const onSelectFile = (e) => {
    setUpImg(null);
    setCropShow(false);
    setLoader(true);
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        let img = new Image();
        img.onload = function () {
          setLoader(false);
          if (this.width < minWidth || this.height < minHeight) {
            setNotification({
              message: `Miniumum width of image should be ${minWidth}px and height should be greater than ${minHeight}px`,
              type: "danger",
            });
          } else {
            setUpImg(reader.result);
            setCropShow(true);
          }
        };
        img.onerror = function () {
          setLoader(false);
          setNotification({
            message: "Not a valid file type",
            type: "danger",
          });
        };
        img.src = reader.result;
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  const handleClick = () => inputEl.current.click();
  return (
    <div>
      <div className="fileinput text-center">
        <input
          type="file"
          accept="image/*"
          onChange={onSelectFile}
          ref={inputEl}
        />
        <div className={`thumbnail ${thumbnailClass}`}>
          <img src={croppedImage || defaultAvatar()} alt="..." />
        </div>
        <div>
          {croppedImage ? (
            <span>
              <Button className="btn-round" onClick={handleClick} size="sm">
                Change
              </Button>
              <Button
                variant="danger"
                className="btn-round"
                onClick={() => onRemove()}
                size="sm"
              >
                <i className="fa fa-times" />
                Remove
              </Button>
            </span>
          ) : (
            <Button className="btn-round" onClick={handleClick} size="sm">
              Select image
            </Button>
          )}
        </div>
      </div>
      <CropModal
        setCropShow={setCropShow}
        cropShow={cropShow}
        upImg={upImg}
        aspect={aspect}
        setCroppedImage={(blob) => {
          setCroppedImage(window.URL.createObjectURL(blob));
          onCrop(blob);
        }}
      />
    </div>
  );
};

ImageApp.propTypes = {
  setNotification: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { setNotification, setLoader })(
  ImageApp
);
