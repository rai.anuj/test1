import React, { useState, useCallback, useRef, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

const Add = ({ cropShow, setCropShow, upImg, aspect, setCroppedImage }) => {
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: "%",
    width: 30,
    aspect,
  });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);
  const selectCrop = () => {
    if (!completedCrop || !imgRef.current) {
      return;
    }
    setCropShow(false);
    const pixelRatio = window.devicePixelRatio || 1;
    const image = imgRef.current;
    const canvas = document.createElement("canvas");
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
    canvas.toBlob(
      (blob) => {
        setCroppedImage(blob);
      },
      "image/jpeg",
      0.9
    );
  };
  return (
    <Modal size="lg" show={cropShow} onHide={() => setCropShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>Crop Image</Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body>
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            onChange={(c) => setCrop(c)}
            onComplete={(c) => setCompletedCrop(c)}
          />
        </Modal.Body>
      </Form>
      <Modal.Footer className="pl4 pr-4">
        <Button
          className="btn-round mr-2"
          variant="default"
          onClick={() => setCropShow(false)}
        >
          Cancel
        </Button>
        <Button
          className="btn-round"
          variant="info"
          onClick={() => {
            selectCrop();
          }}
        >
          Upload
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Add;
