import React from "react";
import { Pagination, Form } from "react-bootstrap";

export default (props) => {
  let {
    gotoPage,
    previousPage,
    nextPage,
    pageCount,
    canPreviousPage,
    canNextPage,
    pageIndex,
    pageSize,
    totalItems,
    setPageSize,
  } = props;
  // default to first page
  let currentPage = pageIndex + 1;

  // default page size is 10
  pageSize = pageSize || 10;

  // calculate total pages
  var totalPages = pageCount;

  var startPage, endPage;
  if (totalPages <= 5) {
    // less than 10 total pages so show all
    startPage = 1;
    endPage = totalPages;
  } else {
    // more than 10 total pages so calculate start and end pages
    if (currentPage <= 3) {
      startPage = 1;
      endPage = 5;
    } else if (currentPage + 2 >= totalPages) {
      startPage = totalPages - 4;
      endPage = totalPages;
    } else {
      startPage = currentPage - 3;
      endPage = currentPage + 2;
    }
  }

  // calculate start and end item indexes
  var startIndex = (currentPage - 1) * pageSize;
  var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

  // create an array of pages to ng-repeat in the pager control
  var pages = [...Array(endPage + 1 - startPage).keys()].map(
    (i) => startPage + i
  );
  return (
    <div className="clearfix">
      <div className="float-left">
        <span className="d-none d-md-inline-block">
          Page <b>{currentPage}</b> of <b>{totalPages}</b>
        </span>
      </div>

      <div className="float-right">
        <span className="pr-2 d-none d-md-inline-block">
          <span className="d-inline-block  pr-2">
            <Form.Control
              as="select"
              value={pageSize}
              onChange={(e) => setPageSize(e.target.value)}
            >
              <option value="5">Show 5</option>
              <option value="10">Show 10</option>
              <option value="20">Show 20</option>
              <option value="25">Show 25</option>
              <option value="30">Show 50</option>
              <option value="100">Show 100</option>
            </Form.Control>
          </span>
        </span>
        <span className="d-inline-block">
          <Pagination>
            <Pagination.Item
              className="d-none d-md-inline-block"
              {...{ disabled: !props.canPreviousPage }}
              onClick={() => gotoPage(0)}
            >
              <i className="fa fa-angle-double-left" />
            </Pagination.Item>

            <Pagination.Item
              {...{ disabled: !props.canPreviousPage }}
              onClick={previousPage}
            >
              <i className="fa fa-angle-left" />
            </Pagination.Item>
            {pages.length > 0 && pages[0] != 1 && (
              <Pagination.Ellipsis className="d-none d-md-inline-block"></Pagination.Ellipsis>
            )}
            {pages.map((page, index) => (
              <Pagination.Item
                key={index}
                onClick={() => gotoPage(page - 1)}
                {...{ active: page == currentPage }}
              >
                {page}
              </Pagination.Item>
            ))}
            {pages.length > 0 && pages[pages.length - 1] != pageCount && (
              <Pagination.Ellipsis className="d-none d-md-inline-block"></Pagination.Ellipsis>
            )}
            <Pagination.Item
              {...{ disabled: !props.canNextPage }}
              onClick={nextPage}
            >
              <i className="fa fa-angle-right" />
            </Pagination.Item>
            <Pagination.Item
              className="d-none d-md-inline-block"
              {...{ disabled: !props.canNextPage }}
              onClick={() => gotoPage(totalPages - 1)}
            >
              <i className="fa fa-angle-double-right" />
            </Pagination.Item>
          </Pagination>
        </span>
      </div>
    </div>
  );
};
