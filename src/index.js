import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faEdit,
  faTrash,
  faPlus,
  faHome,
  faHeart,
  faUserCog,
  faBroadcastTower,
  faShoppingBasket,
  faAngleLeft,
  faAngleRight,
  faBars,
  faSearch,
  faSignOutAlt,
  faPhone,
  faLink,
  faAt,
  faUser,
  faCogs,
  faGlobe,
} from "@fortawesome/free-solid-svg-icons";
import { Provider } from "react-redux";
import store from "./store";

import {
  faFacebookF,
  faTwitter,
  faInstagram,
  faLinkedin,
  faYoutube,
  faWhatsapp,
} from "@fortawesome/free-brands-svg-icons";

import axios from "axios";
import { API_BASE_URL } from "./config";

axios.defaults.baseURL = API_BASE_URL;
library.add(
  faFacebookF,
  faTwitter,
  faInstagram,
  faEdit,
  faTrash,
  faPlus,
  faHome,
  faHeart,
  faUserCog,
  faBroadcastTower,
  faShoppingBasket,
  faAngleLeft,
  faAngleRight,
  faBars,
  faSearch,
  faSignOutAlt,
  faPhone,
  faLink,
  faAt,
  faLinkedin,
  faYoutube,
  faWhatsapp,
  faUser,
  faCogs,
  faGlobe
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
