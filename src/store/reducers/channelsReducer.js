import {
  CHANNELS_LIST,
  CLEAR_STATUS,
  CHANNEL_CREATE_ERROR,
  CHANNEL_CREATE_SUCCESS,
  AUTHORIZED_TENANTS,
} from "../actions/types";

const initialState = {
  list: [],
  createChannelError: null,
  createChannelSuccess: null,
  tenants: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CHANNELS_LIST:
      return {
        ...state,
        list: action.payload,
      };
    case CHANNEL_CREATE_SUCCESS:
      return {
        ...state,
        createChannelSuccess: action.payload || true,
        createChannelError: null,
      };
    case CHANNEL_CREATE_ERROR:
      return {
        ...state,
        createChannelError: action.payload || true,
        createChannelSuccess: null,
      };
    case AUTHORIZED_TENANTS:
      return {
        ...state,
        tenants: action.payload,
      };
    case CLEAR_STATUS:
      return {
        ...state,
        createChannelError: null,
        createChannelSuccess: null,
      };
    default:
      return state;
  }
}
