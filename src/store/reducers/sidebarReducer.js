import { SIDEBAR_TOGGLED } from "../actions/types";

const initialState = { toggled: false };

export default function (state = initialState, action) {
  switch (action.type) {
    case SIDEBAR_TOGGLED:
      return {
        toggled: !state.toggled,
      };
      break;
    default:
      return state;
  }
}
