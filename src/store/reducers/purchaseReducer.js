import { ADD_CART, PURCHASE_LIST } from "../actions/types";

const initialState = { cartCount: 0, list: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_CART:
      return {
        ...state,
        cartCount: state.cartCount + 1,
      };
      break;
    case PURCHASE_LIST:
      return {
        ...state,
        list: action.payload,
      };
      break;
    default:
      return state;
  }
}
