import { combineReducers } from "redux";
import authReduer from "./authReduer";

import sidebarReducer from "./sidebarReducer";
import membersReducer from "./membersReducer";
import ticketsReducer from "./ticketsReducer";
import purchaseReducer from "./purchaseReducer";
import miscReducer from "./miscReducer";
import channelsReducer from "./channelsReducer";
import collectionsReducer from "./collectionsReducer";
import userReducer from "./userReducer";
import adminReducer from "./adminReducer";

export default combineReducers({
  auth: authReduer,
  sidebar: sidebarReducer,
  tickets: ticketsReducer,
  members: membersReducer,
  purchase: purchaseReducer,
  misc: miscReducer,
  channels: channelsReducer,
  collections: collectionsReducer,
  user: userReducer,
  admin: adminReducer,
});
