import { MEMBERS_LIST } from "../actions/types";

const initialState = { list: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case MEMBERS_LIST:
      return {
        list: action.payload,
      };
      break;
    default:
      return state;
  }
}
