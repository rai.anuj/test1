import { COLLECTIONS_LIST } from "../actions/types";

const initialState = {
  collections: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case COLLECTIONS_LIST:
      return {
        ...state,
        collections: action.payload,
      };
    default:
      return state;
  }
}
