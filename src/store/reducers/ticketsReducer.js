import { TICKETS_LIST } from "../actions/types";

const initialState = { list: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case TICKETS_LIST:
      return {
        list: action.payload,
      };
      break;
    default:
      return state;
  }
}
