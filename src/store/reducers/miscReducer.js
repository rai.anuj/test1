import {
  SET_NOTIFICATION,
  SET_LOADER,
  COUNTRIES_LIST,
  CURRENCY_LIST,
} from "../actions/types";

const initialState = {
  notification: { id: 1 },
  showLoader: false,
  countries: [],
  currencies: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_NOTIFICATION:
      return {
        ...state,
        notification: { id: Math.random(), ...action.payload },
      };
    case SET_LOADER:
      return {
        ...state,
        showLoader: action.payload,
      };
    case COUNTRIES_LIST:
      return {
        ...state,
        countries: action.payload,
      };
    case CURRENCY_LIST:
      return {
        ...state,
        currencies: action.payload,
      };
    
    default:
      return state;
  }
}
