import {
  PROFILE_GET,
  PROFILE_SAVED,
  PROFILE_PHOTO_GET,
  PROFILE_PHOTO_SAVED,
  PROFILE_PHOTO_DELETE,
  PROFILE_PASSWORD_CHANGED_LOGGED_IN,
  PROFILE_GET_ERROR,
  PROFILE_SAVED_ERROR,
  PROFILE_PHOTO_GET_ERROR,
  PROFILE_PHOTO_SAVED_ERROR,
  PROFILE_PHOTO_DELETE_ERROR,
  PROFILE_PASSWORD_CHANGED_LOGGED_IN_ERROR,
} from "../actions/types";

const initialState = {
  profile: {},
  photo: null,
  profileSaveError: null,
  profilePhotoSaveError: null,
  profilePhotoDeleteError: null,
  passwordChangeError: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case PROFILE_GET:
      return {
        ...state,
        profile: action.payload,
      };
    case PROFILE_PHOTO_GET:
      return {
        ...state,
        photo: action.payload,
      };
    case PROFILE_SAVED_ERROR:
      return {
        ...state,
        profileSaveError: action.payload,
      };
    case PROFILE_PHOTO_SAVED_ERROR:
      return {
        ...state,
        profilePhotoSaveError: action.payload,
      };
    case PROFILE_PHOTO_DELETE_ERROR:
      return {
        ...state,
        profilePhotoDeleteError: action.payload,
      };
    case PROFILE_PASSWORD_CHANGED_LOGGED_IN_ERROR:
      return {
        ...state,
        passwordChangeError: action.payload,
      };
    default:
      return state;
  }
}
