import {
  ADMIN_MEMBER_LIST,
  ADMIN_MEMBER_TYPE_LIST,
  ADMIN_VIDEO_LIST,
  ADMIN_COLLECTION_LIST,
  ADMIN_COLLECTION_VIDEO_LIST,
  ADMIN_PRICE_LIST,
  ADMIN_ADD_SUCCESS,
  ADMIN_UPDATE_SUCCESS,
  ADMIN_DELETE_SUCCESS,
  ADMIN_ADD_ERROR,
  ADMIN_UPDATE_ERROR,
  ADMIN_DELETE_ERROR,
  CLEAR_STATUS,
} from "../actions/types";

const initialState = {
  members: [],
  memberTypes: [],
  videos: [],
  collections: [],
  collectionVideos: [],
  prices: [],
  addError: null,
  addSuccess: null,
  updateError: null,
  updateSuccess: null,
  deleteError: null,
  deleteSuccess: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADMIN_MEMBER_LIST:
      return {
        ...state,
        members: action.payload,
      };

    case ADMIN_MEMBER_TYPE_LIST:
      return {
        ...state,
        memberTypes: action.payload,
      };
    case ADMIN_VIDEO_LIST:
      return {
        ...state,
        videos: action.payload,
      };
    case ADMIN_COLLECTION_LIST:
      return {
        ...state,
        collections: action.payload,
      };
    case ADMIN_COLLECTION_VIDEO_LIST:
      return {
        ...state,
        collectionVideos: action.payload,
      };
    case ADMIN_PRICE_LIST:
      return {
        ...state,
        prices: action.payload,
      };
    case ADMIN_ADD_SUCCESS:
      return {
        ...state,
        addError: null,
        addSuccess: action.payload,
        updateError: null,
        updateSuccess: null,
        deleteError: null,
        deleteSuccess: null,
      };
    case ADMIN_UPDATE_SUCCESS:
      return {
        ...state,
        addError: null,
        addSuccess: null,
        updateError: null,
        updateSuccess: action.payload,
        deleteError: null,
        deleteSuccess: null,
      };
    case ADMIN_DELETE_SUCCESS:
      return {
        ...state,
        addError: null,
        addSuccess: null,
        updateError: null,
        updateSuccess: null,
        deleteError: null,
        deleteSuccess: action.payload,
      };

    case ADMIN_ADD_ERROR:
      return {
        ...state,
        addError: action.payload,
        addSuccess: null,
        updateError: null,
        updateSuccess: null,
        deleteError: null,
        deleteSuccess: null,
      };
    case ADMIN_UPDATE_ERROR:
      return {
        ...state,
        addError: null,
        addSuccess: null,
        updateError: action.payload,
        updateSuccess: null,
        deleteError: null,
        deleteSuccess: null,
      };
    case ADMIN_DELETE_ERROR:
      return {
        ...state,
        addError: null,
        addSuccess: null,
        updateError: null,
        updateSuccess: null,
        deleteError: action.payload,
        deleteSuccess: null,
      };
    case CLEAR_STATUS:
      return {
        ...state,
        addError: null,
        addSuccess: null,
        updateError: null,
        updateSuccess: null,
        deleteError: null,
        deleteSuccess: null,
      };
    default:
      return state;
  }
}
