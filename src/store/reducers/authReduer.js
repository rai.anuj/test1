import {
  LOGGED_IN,
  LOGGED_OUT,
  REGISTERATION_SUCCESS,
  REGISTERATION_ERROR,
  LOGIN_ERROR,
  RESET_EMAIL_SUCCESS,
  RESET_FAILURE,
  RESET_AUTH_ERROR,
  ACTIVATE_EMAIL_STATE,
} from "../actions/types";

const initialState = {
  registerationDone: false,
  registerationError: false,
  loginError: false,
  resetEmailSent: false,
  resetError: false,
  activateEmailState: "PENDING",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGGED_IN:
      return {
        ...state,
        isAuth: true,
      };
    case LOGGED_OUT:
      return {
        ...state,
        isAuth: false,
      };
    case LOGGED_OUT:
      return {
        ...state,
        isAuth: false,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loginError: true,
      };
    case REGISTERATION_SUCCESS:
      return {
        ...state,
        registerationDone: true,
        registerationError: false,
      };
    case REGISTERATION_ERROR:
      return {
        ...state,
        registerationError: action.payload,
      };
    case RESET_EMAIL_SUCCESS:
      return {
        ...state,
        resetEmailSent: action.payload,
        resetError: false,
      };
    case RESET_FAILURE:
      return {
        ...state,
        resetError: action.payload,
      };
    case RESET_AUTH_ERROR:
      return {
        ...state,
        resetError: false,
        registerationError: false,
        loginError: false,
      };
    case ACTIVATE_EMAIL_STATE:
      return {
        ...state,
        activateEmailState: action.payload,
      };
    default:
      return state;
  }
}
