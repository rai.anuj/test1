import { MEMBERS_LIST } from "./types";
import membersData from "../dummyData/members.json";
export const getMembers = () => (dispatch) => {
  dispatch({
    type: MEMBERS_LIST,
    payload: membersData,
  });
};
