import axios from "axios";

import { COLLECTIONS_LIST } from "./types";

export const getAccessibleCollections = (channelID) => (dispatch) => {
  axios
    .get(`/api/Resultset/PublicCollection/${channelID}`, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      dispatch({
        type: COLLECTIONS_LIST,
        payload: { [channelID]: res.data },
      });
      console.log(res.data);
    })
    .catch((err) => {});
    axios
    .get(`/api/Resultset/MyChannelsCollection`, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
     
      console.log(res.data);
    })
    .catch((err) => {});
    ///api/Resultset/Pricelist/b4e43df9-263f-4e55-a893-4d455a8d14ce/0ce21c62-fe52-4414-af68-2eceb091ed88
};
