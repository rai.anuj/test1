import axios from "axios";

import {
  ADMIN_MEMBER_LIST,
  ADMIN_MEMBER_TYPE_LIST,
  ADMIN_VIDEO_LIST,
  ADMIN_COLLECTION_LIST,
  ADMIN_COLLECTION_VIDEO_LIST,
  ADMIN_PRICE_LIST,
  ADMIN_ADD_SUCCESS,
  ADMIN_UPDATE_SUCCESS,
  ADMIN_DELETE_SUCCESS,
  ADMIN_ADD_ERROR,
  ADMIN_UPDATE_ERROR,
  ADMIN_DELETE_ERROR,
  API_ERROR,
} from "./types";
import { parseError } from "../../utils/error";
import { clearStatus } from "./misc";
//Member types

export const getMemberTypeList = (tenantCode) => (dispatch) => {
  axios
    .get("/api/MemberType", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_MEMBER_TYPE_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const memberTypeAdd = (tenantCode, data) => (dispatch) => {
  dispatch(clearStatus ());
  axios
    .post("/api/MemberType", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_ADD_SUCCESS,
        payload: "Member type added successfully",
      });
      dispatch(getMemberTypeList(tenantCode));
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const memberTypeUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/MemberType/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Member type updated successfully",
      });
      dispatch(getMemberTypeList(tenantCode));
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const memberTypeDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/MemberType/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Member type deleted successfully",
      });
      dispatch(getMemberTypeList(tenantCode));
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};

//Members
export const memberAdd = (tenantCode, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .post("/api/Member", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_ADD_SUCCESS,
        payload: "Member added successfully",
      });
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const getMemberList = (tenantCode) => (dispatch) => {
  axios
    .get("/api/Member", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_MEMBER_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const memberUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/Member/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Member updated successfully",
      });
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const memberDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/Member/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Member deleted successfully",
      });
      dispatch(getMemberList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};

//Videos
export const videoAdd = (tenantCode, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .post("/api/Video", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_ADD_SUCCESS,
        payload: "Video added successfully",
      });
      dispatch(getVideoList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const getVideoList = (tenantCode) => (dispatch) => {
  axios
    .get("/api/Video", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_VIDEO_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const videoUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/Video/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Video updated successfully",
      });
      dispatch(getVideoList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const videoDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/Video/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Video deleted successfully",
      });
      dispatch(getVideoList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};

//Collections

export const getCollectionList = (tenantCode) => (dispatch) => {
  axios
  .get("/api/CollectionVideo?FilterParameter=%7B%22FilterCriteria%22%3A%5B%7B%22PropertyName%22%3A%22VideoId%22%2C%22SearchOperator%22%3A0%2C%22SearchValue%22%3A%22586fb0bc-c642-4ad0-a182-6bbe8491cb87%22%7D%5D%7D", {
    headers: {
      content: "application/json",
      TenantCode: tenantCode,
    },
  })
  .then((res) => {
   
  })
  .catch((err) => {
    dispatch({
      type: API_ERROR,
      payload: true,
    });
  });

  axios
    .get("/api/Collection", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_COLLECTION_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const collectionAdd = (tenantCode, data, videos) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .post("/api/Collection", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      videos.forEach((v) => {
        const pData = {
          CollectionId: res.data.Id,
          CollName: data.CollName,
          AvailFrom: data.AvailFrom,
          AvailTo: data.AvailTo,
          CollVideoStatus: 1,
          VideoId: v.value,
          VideoTitle: v.label,
          VideoDescr: v.VideoDescr,
        };
        dispatch(collectionVideoAdd(tenantCode, pData, true));
      });
      dispatch({
        type: ADMIN_ADD_SUCCESS,
        payload: "Collection added successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const collectionUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/Collection/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Collection updated successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const collectionDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/Collection/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Collection deleted successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};

//CollectionVideos

export const getCollectionVideoList = (tenantCode) => (dispatch) => {
  axios
    .get("/api/CollectionVideo", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_COLLECTION_VIDEO_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const collectionVideoAdd = (tenantCode, data, noDispatch) => (
  dispatch
) => {
  dispatch(clearStatus ());

  axios
    .post("/api/CollectionVideo", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      if (!noDispatch) {
        dispatch({
          type: ADMIN_ADD_SUCCESS,
          payload: "Video added to collection successfully",
        });
      }
      dispatch(getCollectionVideoList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const collectionVideoUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/CollectionVideo/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Collection Video updated successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const collectionVideoDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/CollectionVideo/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Collection Video deleted successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};

//PriceList

export const getPriceList = (tenantCode) => (dispatch) => {
  axios
    .get("/api/PriceList", {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_PRICE_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: API_ERROR,
        payload: true,
      });
    });
};

export const priceListAdd = (tenantCode, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .post("/api/PriceList", data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_ADD_SUCCESS,
        payload: "Price added successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_ADD_ERROR,
        payload: payload,
      });
    });
};

export const priceListUpdate = (tenantCode, id, data) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .put(`/api/PriceList/${id}`, data, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_UPDATE_SUCCESS,
        payload: "Price updated successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_UPDATE_ERROR,
        payload: payload,
      });
    });
};

export const priceListDelete = (tenantCode, id) => (dispatch) => {
  dispatch(clearStatus ());

  axios
    .delete(`/api/PriceList/${id}`, {
      headers: {
        content: "application/json",
        TenantCode: tenantCode,
      },
    })
    .then((res) => {
      dispatch({
        type: ADMIN_DELETE_SUCCESS,
        payload: "Price deleted successfully",
      });
      dispatch(getCollectionList(tenantCode));
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: ADMIN_DELETE_ERROR,
        payload: payload,
      });
    });
};
