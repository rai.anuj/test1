import { TICKETS_LIST } from "./types";
import ticketsData from "../dummyData/tickets.json";
export const getTickets = () => (dispatch) => {
  dispatch({
    type: TICKETS_LIST,
    payload: ticketsData,
  });
};
