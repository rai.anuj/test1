import { ADD_CART, PURCHASE_LIST } from "./types";
import purchaseData from "../dummyData/purchase.json";

export const addToCart = () => (dispatch) => {
  dispatch({
    type: ADD_CART,
  });
};

export const getPurchasedList = () => (dispatch) => {
  dispatch({
    type: PURCHASE_LIST,
    payload: purchaseData,
  });
};
