import axios from "axios";
import {
  SET_NOTIFICATION,
  CLEAR_NOTIFICATIONS,
  SET_LOADER,
  KEEP_ALIVE,
  COUNTRIES_LIST,
  CURRENCY_LIST,
  CLEAR_STATUS,
} from "./types";

export const setNotification = (noti) => (dispatch) => {
  dispatch({
    type: SET_NOTIFICATION,
    payload: noti,
  });
};

export const clearNoti = () => (dispatch) => {
  dispatch({
    type: CLEAR_NOTIFICATIONS,
  });
};

export const setLoader = (show) => (dispatch) => {
  dispatch({
    type: SET_LOADER,
    payload: show,
  });
};

export const keepAlive = () => (dispatch) => {
  axios
    .get(`/api/KeepAlive`)
    .then((res) => {
      dispatch({
        type: KEEP_ALIVE,
      });
    })
    .catch((err) => {});
};

export const getCountries = () => (dispatch) => {
  axios
    .get(`/api/GlobalLovs/Country`)
    .then((res) => {
      dispatch({
        type: COUNTRIES_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {});
};

export const getCurrencies = () => (dispatch) => {
  axios
    .get(`/api/GlobalLovs/Currency`)
    .then((res) => {
      dispatch({
        type: CURRENCY_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {});
};

export const clearStatus = () => (dispatch) => {
  dispatch({
    type: CLEAR_STATUS,
  });
};
