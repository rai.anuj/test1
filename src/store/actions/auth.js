import axios from "axios";

import {
  LOGGED_IN,
  LOGGED_OUT,
  LOGIN_ERROR,
  REGISTERATION_SUCCESS,
  REGISTERATION_ERROR,
  RESET_EMAIL_SUCCESS,
  RESET_FAILURE,
  RESET_AUTH_ERROR,
  ACTIVATE_EMAIL_STATE,
} from "./types";

import { setLoader } from "./misc";
import { getUserProfile, getUserProfilePhoto } from "./user";
import { getAccessibleChannels, getAuthorizedTenants } from "./channels";

import { parseError } from "../../utils/error";

export const checkLogin = () => (dispatch) => {
  let token = window.localStorage.getItem("token");
  //check token
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    dispatch(getUserProfile(true));
    dispatch(getUserProfilePhoto());
    dispatch(getAccessibleChannels());
    dispatch(getAuthorizedTenants());
  } else {
    dispatch({
      type: LOGGED_OUT,
    });
  }
};

export const doRegister = (data) => (dispatch) => {
  dispatch(setLoader(true));
  axios
    .post(`/api/Account/Register`, data, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      dispatch(setLoader(false));
      dispatch({
        type: REGISTERATION_SUCCESS,
        payload: { data: res.data },
      });
    })
    .catch((err) => {
      dispatch(setLoader(false));
      const payload = parseError(err, dispatch);
      dispatch({
        type: REGISTERATION_ERROR,
        payload: payload,
      });
    });
};

export const doActivateEmail = (data) => (dispatch) => {
  dispatch({
    type: ACTIVATE_EMAIL_STATE,
    payload: "PENDING",
  });
  dispatch(setLoader(true));
  axios
    .post(`/api/account/ConfirmEmail`, null, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      params: data,
    })
    .then((res) => {
      dispatch(setLoader(false));
      dispatch({
        type: ACTIVATE_EMAIL_STATE,
        payload: "DONE",
      });
    })
    .catch((err) => {
      dispatch(setLoader(false));
      dispatch({
        type: ACTIVATE_EMAIL_STATE,
        payload: "FAILED",
      });
    });
};

export const doLogin = (data) => (dispatch) => {
  const params = new URLSearchParams();
  params.append("grant_type", "password");
  params.append("username", data.email);
  params.append("password", data.password);
  dispatch(setLoader(true));
  axios
    .post(`/Token`, params, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then((res) => {
      if (res.data.access_token) {
        window.localStorage.setItem("token", res.data.access_token);
        window.localStorage.setItem("username", data.email);
        dispatch(resetAuthError());
        dispatch(checkLogin());
      } else {
        dispatch(setLoader(false));
        dispatch({
          type: LOGIN_ERROR,
          payload: true,
        });
      }
    })
    .catch((err) => {
      dispatch(setLoader(false));
      dispatch({
        type: LOGIN_ERROR,
        payload: true,
      });
    });
};

export const doReset = (email) => (dispatch) => {
  dispatch(setLoader(true));
  axios
    .post(`/api/account/SendForgotPasswordEmail?username=${email}`)
    .then((res) => {
      dispatch(setLoader(false));
      dispatch({
        type: RESET_EMAIL_SUCCESS,
        payload: true,
      });
    })
    .catch((err) => {
      dispatch(setLoader(false));
      dispatch({
        type: RESET_FAILURE,
        payload: true,
      });
    });
};

export const doLogout = () => (dispatch) => {
  window.localStorage.removeItem("token");
  window.localStorage.removeItem("username");

  dispatch({
    type: LOGGED_OUT,
  });
};

export const resetAuthError = () => (dispatch) => {
  dispatch({
    type: RESET_AUTH_ERROR,
  });
};
