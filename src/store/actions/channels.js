import axios from "axios";

import {
  CHANNELS_LIST,
  CHANNEL_CREATE_ERROR,
  CHANNEL_CREATE_SUCCESS,
  AUTHORIZED_TENANTS,
} from "./types";
import { parseError } from "../../utils/error";

export const getAccessibleChannels = () => (dispatch) => {
  const url = axios.defaults.headers.common["Authorization"]
    ? `/api/Resultset/AccessibleChannels`
    : `/api/Resultset/PublicChannels`;
  axios
    .get(url, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      dispatch({
        type: CHANNELS_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {});
};

export const createChannel = (data) => (dispatch) => {
  axios
    .post(
      `/api/tenant`,
      { ...data.business, ...data.contact },
      {
        headers: {
          content: "application/json",
        },
      }
    )
    .then((res) => {
      axios
        .post(`/api/OrgProfile`, data.profile, {
          headers: {
            content: "application/json",
            TenantCode: res.data.TenantCode,
          },
        })
        .then((res) => {
          dispatch({
            type: CHANNEL_CREATE_SUCCESS,
            payload: res.data,
          });
          dispatch(getAccessibleChannels());
          dispatch(getAuthorizedTenants());
        })
        .catch((err) => {
          const payload = parseError(err, dispatch);
          dispatch({
            type: CHANNEL_CREATE_ERROR,
            payload: payload,
          });
        });
    })
    .catch((err) => {
      const payload = parseError(err, dispatch);
      dispatch({
        type: CHANNEL_CREATE_ERROR,
        payload: payload,
      });
    });
};

export const cancelChannelAccount = (id) => (dispatch) => {
  axios
    .put(`/api/tenant/${id}/cancelaccount`, null, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      dispatch({
        type: CHANNELS_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {});
};

export const getAuthorizedTenants = () => (dispatch) => {
  axios
    .get(`/api/Authorization/AuthorizedTenants`, null, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      dispatch({
        type: AUTHORIZED_TENANTS,
        payload: res.data,
      });
    })
    .catch((err) => {});
};
//"raw": "{\r\n  \"TenantCode\": \"MM\",\r\n  \"OrgName\": \"Maximum Media\",\r\n  \"Website\": \"www.maximummedia.in \",\r\n  \"CountryCode\": \"USA\",\r\n  \"Address\": \"Devon Ridge Dr\",\r\n  \"City\": \"Burr Ridge\",\r\n  \"State\": \"IL\",\r\n  \"Postal\": \"60555\",\r\n  \"PhoneCountryCode\": \"+1\",\r\n  \"Phone\": \"(630) 222-5678\",\r\n  \"PrimaryContact\": \"Manimaran VK\",\r\n  \"PrimaryEmail\": \"RNatarajan@unimindss.com\",\r\n  \"PrimaryPhoneCountryCode\": \"+1\",\r\n  \"PrimaryPhone\": \"(630) 786-6630\",\r\n  \"PrimaryMobileCountryCode\": \"\",\r\n  \"PrimaryMobile\": \"\",\r\n  \"BillingContact\": \"Ravi Natarajan\",\r\n  \"BillingEmail\": \"RNatarajan@unimindss.com\",\r\n  \"BillingPhoneCountryCode\": \"\",\r\n  \"BillingPhone\": \"\",\r\n  \"BillingMobileCountryCode\": \"\",\r\n  \"BillingMobile\": \"\",\r\n  \"SubscriptionKey\": \"\",\r\n  \"SubscriptionStatus\": 1,\r\n  \"Id\": \"\",\r\n  \"CreateBy\": \"\",\r\n  \"CreateDttm\": \"\",\r\n  \"LastUpdBy\": \"\",\r\n  \"LastUpdDttm\": \"\",\r\n  \"RowVersion\": 0\r\n}",

//TenantCode: "029722010AE1E53",
