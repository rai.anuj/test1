import axios from "axios";

import {
  LOGGED_IN,
  PROFILE_GET,
  PROFILE_SAVED,
  PROFILE_PHOTO_GET,
  PROFILE_PHOTO_SAVED,
  PROFILE_PHOTO_DELETE,
  PROFILE_PASSWORD_CHANGED_LOGGED_IN,
  PROFILE_SAVED_ERROR,
  PROFILE_PHOTO_GET_ERROR,
  PROFILE_PHOTO_SAVED_ERROR,
  PROFILE_PHOTO_DELETE_ERROR,
  PROFILE_PASSWORD_CHANGED_LOGGED_IN_ERROR,
} from "./types";
import { doLogout } from "./auth";
import { setNotification, setLoader } from "./misc";
import { parseError } from "../../utils/error";

export const getUserProfile = (login_event) => (dispatch) => {
  axios
    .get(`/api/userprofile`)
    .then((res) => {
      dispatch(setLoader(false));
      if (login_event) {
        dispatch({
          type: LOGGED_IN,
        });
      }
      const username = window.localStorage.getItem("username");
      dispatch({
        type: PROFILE_GET,
        payload: { username, ...res.data },
      });
    })
    .catch((err) => {
      dispatch(setLoader(false));
      if (!err.response) {
        dispatch(
          setNotification({
            message: "Problem with connecting  the server",
          })
        );
      } else if (err.response.status == 400) {
        dispatch(doLogout());
        dispatch(setNotification({ message: err.response.statusText }));
      } else {
        dispatch(doLogout());
      }
    });
};

export const saveUserProfile = (data, loader) => (dispatch) => {
  loader && dispatch(setLoader(true));
  dispatch({
    type: PROFILE_SAVED_ERROR,
    payload: false,
  });
  axios
    .post(`/api/userprofile`, data, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      loader && dispatch(setLoader(false));
      dispatch({
        type: PROFILE_SAVED,
      });
      dispatch(getUserProfile());
      dispatch(setNotification({ message: "Profile updated successfully" }));
    })
    .catch((err) => {
      loader && dispatch(setLoader(false));
      const payload = parseError(err, dispatch);
      dispatch({
        type: PROFILE_SAVED_ERROR,
        payload: payload,
      });
    });
};

export const getUserProfilePhoto = () => (dispatch) => {
  axios
    .get(`/api/UserProfile/GetProfilePhoto`, {
      responseType: "arraybuffer",
    })
    .then((res) => {
      const img =
        "data:image/jpeg;base64," +
        Buffer.from(res.data, "binary").toString("base64");

      dispatch({
        type: PROFILE_PHOTO_GET,
        payload: img,
      });
    })
    .catch((err) => {
      dispatch({
        type: PROFILE_PHOTO_GET,
        payload: null,
      });
    });
};

export const saveUserProfilePhoto = (file, loader) => (dispatch) => {
  var formdata = new FormData();
  formdata.append("file", file, file.filename);

  loader && dispatch(setLoader(true));
  dispatch({
    type: PROFILE_PHOTO_SAVED_ERROR,
    payload: false,
  });
  axios
    .post(`/api/UserProfile/UploadProfilePhoto`, formdata)
    .then((res) => {
      loader && dispatch(setLoader(false));
      dispatch({
        type: PROFILE_PHOTO_SAVED,
      });
      dispatch(
        setNotification({ message: "Profile photo updated successfully" })
      );

      dispatch(getUserProfilePhoto());
    })
    .catch((err) => {
      loader && dispatch(setLoader(false));
      const payload = parseError(err, dispatch);
      dispatch({
        type: PROFILE_PHOTO_SAVED_ERROR,
        payload: payload,
      });
    });
};

export const deleteUserProfilePhoto = (loader) => (dispatch) => {
  loader && dispatch(setLoader(true));
  dispatch({
    type: PROFILE_PHOTO_DELETE_ERROR,
    payload: false,
  });
  axios
    .delete(`/api/UserProfile/DeleteProfilePhoto`)
    .then((res) => {
      loader && dispatch(setLoader(false));
      dispatch({
        type: PROFILE_PHOTO_DELETE,
      });
      dispatch(getUserProfilePhoto());
    })
    .catch((err) => {
      loader && dispatch(setLoader(false));
      const payload = parseError(err, dispatch);
      dispatch({
        type: PROFILE_PHOTO_DELETE_ERROR,
        payload: payload,
      });
    });
};

export const doChangePassword = (data, loader) => (dispatch) => {
  loader && dispatch(setLoader(true));
  dispatch({
    type: PROFILE_PASSWORD_CHANGED_LOGGED_IN_ERROR,
    payload: false,
  });
  axios
    .post(`/api/Account/ChangePassword`, data, {
      headers: {
        content: "application/json",
      },
    })
    .then((res) => {
      loader && dispatch(setLoader(false));
      dispatch({
        type: PROFILE_PASSWORD_CHANGED_LOGGED_IN,
      });

      dispatch(setNotification({ message: "Password changed successfully" }));
    })
    .catch((err) => {
      loader && dispatch(setLoader(false));
      const payload = parseError(err, dispatch);
      dispatch({
        type: PROFILE_PASSWORD_CHANGED_LOGGED_IN_ERROR,
        payload: payload,
      });
    });
};
//"{{url}}/api/UserProfile/GetProfilePhoto" -get

//"{{url}}/api/UserProfile/UploadProfilePhoto" -post formdata {key src type="file"}

//"{{url}}/api/UserProfile/DeleteProfilePhoto"  - delete

//"{{url}}/api/Account/ChangePassword"  - post "{ "oldPassword": "Welcome1!", "newPassword": "Welcome1!", "confirmPassword": "Welcome1!" }"
//app/json
