import { SIDEBAR_TOGGLED } from "./types";

export const toggleSidebar = () => (dispatch) => {
  dispatch({
    type: SIDEBAR_TOGGLED,
    payload: Math.random(),
  });
};
