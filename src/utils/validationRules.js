export const emailRule = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
export const websiteRule = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+\.[a-z]+(\/[a-zA-Z0-9#]+\/?)*$/;
export const fbRule = /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/;
export const instaRule = /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_]+)/im;
export const youtubeRule = /^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/;
export const linkedinRule = /^http(s)?:\/\/(www\.)?linkedin\.com\/in\/.*$/;
export const phoneRule = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
export const twitterRule = /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/;

export const videoSourceRegEx = {
  Vimeo: /^[0-9a-z]*$/,
  youTube: /^[A-Za-z0-9_\-]{11}$/,
};
