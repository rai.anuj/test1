import React from "react";
import { IMAGE_BASE_URL } from "../config";
import defaultAvatarImg from "../assets/images/default-avatar.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
export const getChannelImage = (images, type) => {
  if (images && Array.isArray(images)) {
    let logo = images.filter((i) => i.fileuseidentifier == type);
    if (logo && logo.length > 0) {
      return IMAGE_BASE_URL + logo[0].filename;
    }
  }
  return false;
};

export const defaultAvatar = () => defaultAvatarImg;

export const getSocial = (c) => {
  const social = [
    {
      key: "twitter",
      icon: <FontAwesomeIcon icon={["fab", "twitter"]} />,
      class: "btn-twitter",
    },
    {
      key: "instagram",
      icon: <FontAwesomeIcon icon={["fab", "instagram"]} />,
      class: "btn-instagram",
    },
    {
      key: "facebook",
      icon: <FontAwesomeIcon icon={["fab", "facebook-f"]} />,
      class: "btn-facebook",
    },
    {
      key: "linkedin",
      icon: <FontAwesomeIcon icon={["fab", "linkedin"]} />,
      class: "btn-linkedin",
    },
    {
      key: "phone",
      icon: <FontAwesomeIcon icon={["fas", "phone"]} />,
      class: "btn-phone",
      prefix: "tel:",
    },
    {
      key: "website",
      icon: <FontAwesomeIcon icon={["fas", "globe"]} />,
      class: "btn-info",
    },
    {
      key: "whatsapp",
      icon: <FontAwesomeIcon icon={["fab", "whatsapp"]} />,
      class: "btn-success",
      prefix: "tel:",
    },
    {
      key: "youtube",
      icon: <FontAwesomeIcon icon={["fab", "youtube"]} />,
      class: "btn-youtube",
    },
  ];

  return (
    <div className="social float-md-left">
      {social.map(
        (s) =>
          c[s.key] && (
            <OverlayTrigger
              key={s.key}
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={<Tooltip id="button-tooltip">{c[s.key]}</Tooltip>}
            >
              <button
                style={{ display: "table-cell" }}
                onClick={() =>
                  window.open(
                    (s.prefix || "") + c[s.key],
                    "_blank",
                    "noopener,noreferrer"
                  )
                }
                target="_blank"
                rel="noopener noreferrer"
                className={`btn btn-icon btn-round ${s.class}  mt-0 mb-0 ml-1 mr-1`}
              >
                {s.icon}
              </button>
            </OverlayTrigger>
          )
      )}
    </div>
  );
};
