import React from "react";
import { setNotification } from "../store/actions/misc";

export const parseError = (err, dispatch) => {
  let payload = true;
  if (!err.response && dispatch) {
    dispatch(
      setNotification({
        message: "Problem with connecting  the server",
      })
    );
    return null;
  }
  if (err.response && err.response.data) {
    if (err.response.data.ModelState) {
      payload = [];
      for (const [key, value] of Object.entries(err.response.data.ModelState)) {
        if (Array.isArray(value)) {
          value.forEach((v) => payload.push(v));
        } else {
          payload.push(value);
        }
      }
    } else if (err.response.data.ReasonPhrase) {
      payload = err.response.data.ReasonPhrase;
    }
  }
  return payload;
};

export const buildErrorMsg = (err) => {
  {
    if (Array.isArray(err)) {
      return (
        <ul>
          {err.map((e) => (
            <li>{e}</li>
          ))}
        </ul>
      );
    } else if (typeof err == "string") {
      return err;
    }
    return <b>Something went wrong</b>;
  }
};
