export const memberStatus = [
  { label: "Active", value: 1 },
  { label: "Due Lapsed", value: 5 },
  { label: "Inactive", value: 10 },
];

export const videoSource = [
  { label: "YouTube", value: 5 },
  { label: "Vimeo", value: 10 },
];

export const rowStatus = [
  { label: "Draft", value: 1 },
  { label: "Active", value: 5 },
  { label: "Inactive", value: 10 },
];

export const priceStatus = [
    { label: "Draft", value: 1 },
    { label: "Active", value: 5 },
    { label: "Soldout", value: 6 },
    { label: "Inactive", value: 10 },
  ];

export const tenantSubscriptionStatus = [
  { label: "Active", value: 1 },
  { label: "Hold", value: 5 },
  { label: "Cancelled", value: 9 },
];
export const getStatus = (type, key) => type.find((t) => t.value == key).label;
