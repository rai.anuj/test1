import React, { useState } from "react";
import { Modal, Button, Form, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from "react-select";

const Add = (props) => {
  const [selPVideos, setPVideos] = useState([]);

  return (
    <Modal
      size="lg"
      show={props.addShow}
      onHide={() => props.setAddShow(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>Add Price</Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Price Name</Form.Label>
            <Form.Control type="text" placeholder="Enter Price Name" />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows="5"
              placeholder="Enter Description"
            />
          </Form.Group>
          <Form.Group className="form-check">
            <label className="form-check-label">
              <input type="checkbox" className="form-check-input" />
              Entire Collection <span className="form-check-sign"></span>
            </label>
          </Form.Group>
          <Form.Group>
            <Form.Label>Videos</Form.Label>
            <Select
              className="react-select default"
              classNamePrefix="react-select"
              placeholder="Choose Videos"
              name="multipleSelect"
              closeMenuOnSelect={true}
              isMulti
              value={selPVideos}
              onChange={(value) => setPVideos(value)}
              options={props.selVideos}
            />
          </Form.Group>
          <Form.Group className="form-check">
            <label className="form-check-label">
              <input type="checkbox" className="form-check-input" />
              Member Only <span className="form-check-sign"></span>
            </label>
          </Form.Group>
          <Form.Group>
            <label className="form-check-label">
              <div className="pb-2">Allowed Type</div>
              <div>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      Life Member <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      Annual Member <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      Non Member <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
              </div>
            </label>
          </Form.Group>
          <Form.Group>
            <label className="form-check-label">
              <div className="pb-2">Allowed Region</div>
              <div>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      India <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      USA <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      Europe <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
                <span className="d-inline-block pr-4">
                  <Form.Group className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" />
                      Middle East <span className="form-check-sign"></span>
                    </label>
                  </Form.Group>
                </span>
              </div>
            </label>
          </Form.Group>
          <Form.Group>
            <Form.Label>Max Tickets</Form.Label>
            <Form.Control type="number" placeholder="Maximum tickets allowed" />
          </Form.Group>
          <Form.Group>
            <Form.Label>Max Views/Ticket</Form.Label>
            <Form.Control
              type="number"
              placeholder="Maximum views per ticket allowed"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Row>
              <Col md="5">
                <Form.Control type="number" placeholder="Price" />
              </Col>
              <Col md="3">
                <Form.Control as="select">
                  <option>INR</option>
                  <option>USD</option>
                  <option>EUR</option>
                  <option>GBP</option>
                </Form.Control>
              </Col>
              <Col md="4">
                <Button size="sm" variant="default">
                  Issue Tickets
                </Button>
              </Col>
            </Row>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Status</Form.Label>
            <div>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                    checked
                  />
                  Draft <span className="form-check-sign"></span>
                </label>
              </span>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                  />
                  Active <span className="form-check-sign"></span>
                </label>
              </span>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                  />
                  Inactive <span className="form-check-sign"></span>
                </label>
              </span>
            </div>
          </Form.Group>
        </Modal.Body>
      </Form>
      <Modal.Footer className="pl4 pr-4">
        <Button
          className="btn-round mr-2"
          variant="default"
          onClick={() => props.setAddShow(false)}
        >
          Cancel
        </Button>
        <Button
          className="btn-round"
          variant="info"
          onClick={() => props.setAddShow(false)}
        >
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Add;
