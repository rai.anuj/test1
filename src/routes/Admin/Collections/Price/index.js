import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Add from "./Add";
import SweetAlert from "react-bootstrap-sweetalert";

const Members = ({ selVideos }) => {
  const [addShow, setAddShow] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [priceList, setPriceList] = useState([]);
  return (
    <div>
      <div className="clearfix">
        <div className="float-right">
          <Button variant="success" size="sm" onClick={() => setAddShow(true)}>
            <FontAwesomeIcon icon={["fas", "plus"]} /> Add
          </Button>
        </div>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr className="text-primary">
            <th>Name</th>
            <th>Collection/Video</th>
            <th>Member</th>
            <th>Amount</th>
            <th style={{ width: "50px" }}>Action</th>
          </tr>
        </thead>
        <tbody></tbody>
      </Table>
      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => setDeleteConfirm(false)}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
      <Add setAddShow={setAddShow} addShow={addShow} selVideos={selVideos} />
    </div>
  );
};

export default Members;
