import React, { useState, useEffect } from "react";
import { Modal, Button, Form, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from "react-select";
import SimpleFileButton from "../../../../components/FileUpload/SimpleFileButton";
import ReactDatetime from "react-datetime";
import Price from "../Price";

import { buildErrorMsg } from "../../../../utils/error";
import { useForm, Controller } from "react-hook-form";
import { inputComponent } from "../../../../components/Form/Input";
import moment from "moment";

const Add = ({
  addShow,
  setAddShow,
  selData,
  onSubmit,
  actionError,
  videos,
  collectionVideos,
}) => {
  const { register, handleSubmit, errors, control } = useForm(); // initialize the hook
  const [selVideos, setSelVideos] = useState([]);
  const options = videos.map((v) => ({
    value: v.Id,
    label: v.VideoTitle,
    VideoDescr: v.VideoDescr,
  }));
  const onData = (data) => {
    onSubmit(data, selVideos);
  };
  useEffect(() => {
    setSelVideos(
      collectionVideos.map((cv) => ({
        value: cv.VideoId,
        label: cv.VideoTitle,
        VideoDescr: cv.VideoDescr,
      }))
    );
  }, [collectionVideos]);
  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          {Object.keys(selData).length > 0 ? "Edit" : "Add"} Collection
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit(onData)}>
        <Modal.Body>
          {actionError && (
            <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
          )}
          {inputComponent({
            name: "CollName",
            type: "text",
            label: "Collection Name",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>CollName</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.CollName || "",
          })}
          {inputComponent({
            name: "Descr",
            type: "text",
            label: "Description",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Description</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.Descr || "",
          })}
          <Form.Group className="form-check">
            <label className="form-check-label">
              <input
                name="AllowNonMember"
                type="checkbox"
                className="form-check-input"
                defaultChecked={selData.AllowNonMember}
                ref={register({
                  validation: {
                    required: false,
                  },
                })}
              />
              Allow Non Member? <span className="form-check-sign"></span>
            </label>
          </Form.Group>

          <Form.Group>
            <Form.Label>Available Dates</Form.Label>
            <Row>
              <Col sm="12" md="6">
                <Form.Label>From</Form.Label>
                <div className={errors["AvailFrom"] && "has-danger"}>
                  <Controller
                    control={control}
                    name="AvailFrom"
                    defaultValue={
                      selData.AvailFrom &&
                      moment(selData.AvailFrom).toISOString()
                    }
                    rules={{ required: true }}
                    render={(props) => (
                      <ReactDatetime
                        initialValue={
                          selData.AvailFrom && moment(selData.AvailFrom)
                        }
                        inputProps={{
                          className: "form-control",
                          placeholder: "From",
                        }}
                        onChange={(val) => {
                          props.onChange(val.toISOString());
                        }}
                      />
                    )}
                  />
                  {errors["AvailFrom"] && (
                    <label className="error text-danger">
                      Date is required
                    </label>
                  )}
                </div>
              </Col>
              <Col sm="12" md="6">
                <Form.Label>To</Form.Label>
                <div className={errors["AvailTo"] && "has-danger"}>
                  <Controller
                    control={control}
                    name="AvailTo"
                    defaultValue={
                      selData.AvailTo && moment(selData.AvailTo).toISOString()
                    }
                    rules={{ required: true }}
                    render={(props) => (
                      <ReactDatetime
                        initialValue={
                          selData.AvailTo && moment(selData.AvailTo)
                        }
                        inputProps={{
                          className: "form-control",
                          placeholder: "To",
                        }}
                        onChange={(val) => {
                          props.onChange(val.toISOString());
                        }}
                      />
                    )}
                  />
                  {errors["AvailTo"] && (
                    <label className="error text-danger">
                      Date is required
                    </label>
                  )}
                </div>
              </Col>
            </Row>
          </Form.Group>
          <Form.Group>
            <Form.Label>Videos</Form.Label>
            <Select
              className="react-select default"
              classNamePrefix="react-select"
              placeholder="Choose Videos"
              name="multipleSelect"
              closeMenuOnSelect={true}
              isMulti
              value={selVideos}
              onChange={(value) => setSelVideos(value)}
              options={options}
            />
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Status</Form.Label>
            <div>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                    checked
                  />
                  Draft <span className="form-check-sign"></span>
                </label>
              </span>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                  />
                  Active <span className="form-check-sign"></span>
                </label>
              </span>
              <span className="form-check-radio d-inline-block pr-4">
                <label className="form-check-label">
                  <input
                    name="collStatus"
                    type="radio"
                    className="form-check-input"
                  />
                  Inactive <span className="form-check-sign"></span>
                </label>
              </span>
            </div>
          </Form.Group>

          <Form.Group
            controlId="exampleForm.ControlSelect1"
            className="file-select-4 d-inline-block"
          >
            <SimpleFileButton
              label="Cover Image"
              btnclassName="default"
              className="mr-1"
            />
          </Form.Group>
          <div>{<Price selVideos={selVideos} />}</div>
        </Modal.Body>

        <Modal.Footer className="pl4 pr-4">
          <Button
            className="btn-round mr-2"
            variant="danger"
            onClick={() => setAddShow(false)}
          >
            Delete
          </Button>
          <Button
            className="btn-round mr-2"
            variant="default"
            onClick={() => setAddShow(false)}
          >
            Cancel
          </Button>
          <Button className="btn-round" variant="info" type="submit">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default Add;
