import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Table,
  Button,
  Card,
  Row,
  Col,
  Figure,
  Container,
  Image,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import {
  collectionAdd,
  collectionUpdate,
  collectionDelete,
} from "../../../../store/actions/admin";
import defaultImg from "../../../../assets/images/app-logo.png";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Add from "./Add";

const Collections = (props) => {
  const [addShow, setAddShow] = useState(false);
  const [selData, setSelData] = useState({});
  useEffect(() => {
    setAddShow(false);
    setSelData({});
  }, [props.addSuccess, props.updateSuccess]);

  const getVideoCount = (collId) =>
    props.collectionVideos.filter((cv) => cv.CollectionId == collId).length;

  return (
    <Container fluid className="figure-fluid">
      <div>
        <div className="clearfix">
          <div className="float-right">
            <Button
              variant="success"
              size="sm"
              onClick={() => setAddShow(true)}
            >
              <FontAwesomeIcon icon={["fas", "plus"]} /> Add
            </Button>
          </div>
        </div>
        <Card className="responsive">
          <Card.Header>
            <Card.Title>Collections</Card.Title> <hr />
          </Card.Header>
          <Card.Body>
            <Row>
              {props.collections.map((v, k) => (
                <Col
                  key={k + 1}
                  xs={12}
                  sm={6}
                  md={3}
                  className="p-4 cursor-pointer"
                  onClick={() => {
                    setSelData(v);
                    setAddShow(true);
                  }}
                >
                  <Card className="bg-white">
                    <Card.Body>
                      <Image
                        fluid
                        alt="Collection"
                        src="https://via.placeholder.com/512"
                      />
                    </Card.Body>
                    <Card.Footer className="footer-2 p-2 pb-3">
                      <Card.Text>
                        {v.CollName}({getVideoCount(v.Id)} Videos)
                      </Card.Text>
                      <div
                        className="text-center text-default"
                        style={{ fontSize: "12px" }}
                      >
                        {v.Descr}
                      </div>
                    </Card.Footer>
                  </Card>
                </Col>
              ))}
            </Row>
          </Card.Body>
        </Card>
      </div>
      <Add
        setAddShow={setAddShow}
        addShow={addShow}
        videos={props.videos}
        collectionVideos={
          Object.keys(selData).length > 0
            ? props.collectionVideos.filter(
                (cv) => cv.CollectionId == selData.Id
              )
            : []
        }
        actionError={props.addError || props.updateError}
        selData={selData}
        onSubmit={(data, videos) => {
          data.CollStatus = 1;
          console.log(data);
          if (props.tenant.TenantCode) {
            if (selData && selData.Id) {
              data.Id = selData.Id;
              data.RowVersion = selData.RowVersion;
              props.collectionUpdate(
                props.tenant.TenantCode,
                selData.Id,
                data,
                videos
              );
            } else {
              props.collectionAdd(props.tenant.TenantCode, data, videos);
            }
          }
        }}
      />
    </Container>
  );
};

Collections.propTypes = {
  collections: PropTypes.array.isRequired,
  collectionVideos: PropTypes.array.isRequired,
  videos: PropTypes.array.isRequired,
  collectionAdd: PropTypes.func.isRequired,
  collectionUpdate: PropTypes.func.isRequired,
  collectionDelete: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  collections: state.admin.collections,
  collectionVideos: state.admin.collectionVideos,
  videos: state.admin.videos,
  addError: state.admin.addError,
  addSuccess: state.admin.addSuccess,
  updateError: state.admin.updateError,
  updateSuccess: state.admin.updateSuccess,
  deleteError: state.admin.deleteError,
  deleteSuccess: state.admin.deleteSuccess,
});

export default connect(mapStateToProps, {
  collectionUpdate,
  collectionDelete,
  collectionAdd,
})(Collections);
