import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Collections from "./Collections";
import Price from "./Price";

const Index = (props) => {
  const [addShow, setAddShow] = useState(false);

  return (
    <div>
      <Collections tenant={props.tenant} />
    </div>
  );
};

export default Index;
