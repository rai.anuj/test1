import React from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { buildErrorMsg } from "../../../utils/error";
import { useForm } from "react-hook-form";
import { inputComponent } from "../../../components/Form/Input";

const Add = ({ addShow, setAddShow, selData, onSubmit, actionError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook

  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          {Object.keys(selData).length > 0 ? "Edit" : "Add"} Member Type
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Modal.Body>
          {actionError && (
            <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
          )}
          {inputComponent({
            name: "Name",
            type: "text",
            label: "Memebrship Type Name",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Membership Type Name</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.Name || "",
          })}
          {inputComponent({
            name: "Description",
            type: "textarea",
            label: "Description",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Description</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.Description || "",
          })}
          <Form.Group className="form-check">
            <label className="form-check-label">
              <input
                name="IsActive"
                type="checkbox"
                className="form-check-input"
                defaultChecked={selData.IsActive === true}
                ref={register({
                  validation: {
                    required: false,
                  },
                })}
              />
              Is Active? <span className="form-check-sign"></span>
            </label>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer className="pl4 pr-4">
          <Button
            className="btn-round mr-2"
            variant="default"
            onClick={() => setAddShow(false)}
          >
            Cancel
          </Button>
          <Button type="submit" className="btn-round" variant="info">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default Add;
