import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import Add from "./Add";
import SweetAlert from "react-bootstrap-sweetalert";
import Table from "./Table";
import {
  memberTypeAdd,
  memberTypeUpdate,
  memberTypeDelete,
} from "../../../store/actions/admin";

const MembershipType = (props) => {
  const [addShow, setAddShow] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [selected, setSelected] = useState({});

  useEffect(() => {
    setAddShow(false);
    setSelected({});
  }, [props.addSuccess, props.updateSuccess]);

  return (
    <div>
      <Table
        data={props.memberTypes}
        onDelete={() => setDeleteConfirm(true)}
        onEdit={(obj) => {
          setSelected(obj);
          setAddShow(true);
        }}
        onAddClick={() => {
          setSelected({});
          setAddShow(true);
        }}
      />
      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => setDeleteConfirm(false)}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
      <Add
        actionError={props.addError || props.updateError}
        setAddShow={setAddShow}
        addShow={addShow}
        selData={selected}
        onSubmit={(data) => {
          if (props.tenant.TenantCode) {
            if (selected && selected.Id) {
              props.memberTypeUpdate(
                props.tenant.TenantCode,
                selected.Id,
                data
              );
            } else {
              props.memberTypeAdd(props.tenant.TenantCode, data);
            }
          }
        }}
      />
    </div>
  );
};

MembershipType.propTypes = {
  memberTypeAdd: PropTypes.func.isRequired,
  memberTypeUpdate: PropTypes.func.isRequired,
  memberTypes: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  memberTypes: state.admin.memberTypes,
  addError: state.admin.addError,
  addSuccess: state.admin.addSuccess,
  updateError: state.admin.updateError,
  updateSuccess: state.admin.updateSuccess,
  deleteError: state.admin.deleteError,
  deleteSuccess: state.admin.deleteSuccess,
});

export default connect(mapStateToProps, {
  memberTypeUpdate,
  memberTypeAdd,
})(MembershipType);
