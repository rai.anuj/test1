import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { setNotification } from "../../../store/actions/misc";
import {
  inputComponent,
  inputGroupPrepend,
} from "../../../components/Form/Input";
import ImageUpload from "../../../components/ImageUpload";
import { buildErrorMsg } from "../../../utils/error";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import {
  emailRule,
  websiteRule,
  fbRule,
  instaRule,
  phoneRule,
  youtubeRule,
  linkedinRule,
  twitterRule,
} from "../../../utils/validationRules";

const Step = React.forwardRef(({ afterValidate }, ref) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook

  const [phone, setPhone] = useState();
  const onSubmit = (data) => {
    data.Phone = (phone && phone.phoneWithCode) || "";
    afterValidate(data, "profile");
  };
  const profile = {};
  return (
    <form ref={ref} onSubmit={handleSubmit(onSubmit)}>
      <h6 className="text-default">Organization Detail</h6>
      {inputComponent({
        name: "OrgName",
        type: "text",
        label: "Channel Name",
        validation: {
          required: true,
        },
        defaultError: (
          <span>
            Please provide valid <b>Channel Name</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      {inputComponent({
        name: "Descr",
        type: "textarea",
        label: "About Channel",
        validation: {
          required: true,
        },
        defaultError: (
          <span>
            Please provide valid <b>description</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      {inputComponent({
        name: "Address",
        type: "text",
        label: "Address",
        placeHolder: "",
        validation: {
          required: false,
        },
        defaultError: "Please provide valid address",
        register,
        errors,
        defaultValue: profile.AltEmail || "",
      })}
      {inputComponent({
        name: "ServiceLocations",
        type: "text",
        label: "Service Locations",
        placeHolder: "",
        validation: {
          required: false,
        },
        defaultError: "Please provide valid location",
        register,
        errors,
        defaultValue: profile.AltEmail || "",
      })}

      <Row>
        <Col md="12" lg="6" xl="4">
          {inputComponent({
            name: "Email",
            type: "email",
            label: "Email",
            placeHolder: "",
            validation: {
              required: true,
              pattern: emailRule,
            },
            defaultError: "Please provide valid email",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
          })}
        </Col>

        <Col md="12" lg="6" xl="4">
          {inputComponent({
            name: "Website",
            type: "text",
            label: "Website",
            placeHolder: "",
            validation: {
              required: false,
              pattern: websiteRule,
            },
            defaultError: "Please provide valid website",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          <Form.Label>Phone</Form.Label>
          <PhoneInput
            country={"us"}
            value={phone && phone.phone}
            onChange={(phoneWithCode, country, e, formattedValue) => {
              const phoneWithoutCode = phoneWithCode.replace(
                new RegExp(`\^${country.dialCode}`),
                ""
              );
              setPhone({
                phoneWithoutCode,
                phone: phoneWithCode,
                countryCode: `+${country.dialCode}`,
              });
            }}
          />
        </Col>
      </Row>
      <hr />
      <h6 className="text-default">Social Links</h6>
      <Row>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "Facebook",
            type: "text",
            label: "Facebook Page Url",
            validation: {
              required: false,
              pattern: fbRule,
            },
            defaultError: "Please provide valid Facebook Url",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "facebook-f"],
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "YouTube",
            type: "text",
            label: "YouTube Page Url",
            validation: {
              required: false,
              pattern: youtubeRule,
            },
            defaultError: "Please provide valid link",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "youtube"],
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "LinkedIn",
            type: "text",
            label: "LinkedIn profile Url",
            validation: {
              required: false,
              pattern: linkedinRule,
            },
            defaultError: "Please provide valid url",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "linkedin"],
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "Instagram",
            type: "text",
            label: "Instagram Url",
            validation: {
              required: false,
              pattern: instaRule,
            },
            defaultError: "Please provide valid url",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "instagram"],
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "Twitter",
            type: "text",
            label: "Twitter Url",
            validation: {
              required: false,
              pattern: twitterRule,
            },
            defaultError: "Please provide valid url",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "twitter"],
          })}
        </Col>
        <Col md="12" lg="6" xl="4">
          {inputGroupPrepend({
            name: "WhatsApp",
            type: "text",
            label: "WhatsApp Number",
            validation: {
              required: false,
              pattern: phoneRule,
            },
            defaultError: "Please provide valid phone number",
            register,
            errors,
            defaultValue: profile.AltEmail || "",
            icon: ["fab", "whatsapp"],
          })}
        </Col>
      </Row>
    </form>
  );
});

export default Step;
