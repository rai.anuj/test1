import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import ReactWizard from "../../../components/Wizard";

import Step1 from "./BusinessDetails";
import Step2 from "./Contact";
import Step3 from "./Profile";
import { Container, Card } from "react-bootstrap";
import { buildErrorMsg } from "../../../utils/error";
import { setNotification, clearStatus } from "../../../store/actions/misc";
import { createChannel } from "../../../store/actions/channels";

var steps = [
  {
    stepName: "Organization",
    stepIcon: "nc-icon nc-pin-3",
    component: Step1,
  },
  {
    stepName: "Contact",
    stepIcon: "fa fa-address-book",
    component: Step2,
  },
  {
    stepName: "Profile",
    stepIcon: "fa fa-link",
    component: Step3,
  },
];

const CreateChannel = ({
  createChannel,
  createChannelError,
  createChannelSuccess,
  setNotification,
  clearStatus,
}) => {
  useEffect(() => {
    if (createChannelSuccess) {
      setNotification({ message: "Channel created successfully" });
      clearStatus();
    }
  }, [createChannelSuccess]);
  useEffect(() => {
    if (createChannelError) {
      window.scrollTo(0, 0);
    }
  }, [createChannelError]);
  if (createChannelSuccess) return <Redirect to="/admin" />;

  return (
    <Container fluid>
      {createChannelError && (
        <div className="text-danger pb-4">
          {buildErrorMsg(createChannelError)}
        </div>
      )}
      <ReactWizard
        steps={steps}
        navSteps
        validate
        title="Create Channel"
        headerTextCenter
        finishButtonClasses="btn-wd"
        nextButtonClasses="btn-wd"
        previousButtonClasses="btn-wd"
        finishButtonClick={createChannel}
      />
    </Container>
  );
};
CreateChannel.propTypes = {
  createChannel: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  clearStatus: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  createChannelError: state.channels.createChannelError,
  createChannelSuccess: state.channels.createChannelSuccess,
});

export default connect(mapStateToProps, {
  createChannel,
  setNotification,
  clearStatus,
})(CreateChannel);
