import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm, Controller } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { setNotification } from "../../../store/actions/misc";
import { inputComponent } from "../../../components/Form/Input";
import ImageUpload from "../../../components/ImageUpload";
import { buildErrorMsg } from "../../../utils/error";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import { websiteRule, emailRule } from "../../../utils/validationRules";

const Step = React.forwardRef(
  ({ wizardData, afterValidate, countries }, ref) => {
    const { register, handleSubmit, errors, watch, control } = useForm(); // initialize the hook

    const [phone, setPhone] = useState();
    const onSubmit = (data) => {
      data.PhoneCountryCode = (phone && phone.countryCode) || "";
      data.Phone = (phone && phone.phoneWithoutCode) || "";
      afterValidate(data, "business");
    };
    const profile = {};
    return (
      <form ref={ref} onSubmit={handleSubmit(onSubmit)}>
        <h6 className="text-default">Organization Detail</h6>
        {inputComponent({
          name: "OrgName",
          type: "text",
          label: "Organization Name",
          validation: {
            required: true,
          },
          defaultError: (
            <span>
              Please provide valid <b>Organization Name</b>
            </span>
          ),
          register,
          errors,
          defaultValue: profile.FirstName || "",
        })}
        {inputComponent({
          name: "Address",
          type: "text",
          label: "Address",
          placeHolder: "",
          validation: {
            required: true,
          },
          defaultError: "Please provide valid address",
          register,
          errors,
          defaultValue: profile.AltEmail || "",
        })}
        {inputComponent({
          name: "City",
          type: "text",
          label: "City",
          placeHolder: "",
          validation: {
            required: true,
          },
          defaultError: "Please provide valid city",
          register,
          errors,
          defaultValue: profile.AltEmail || "",
        })}
        {inputComponent({
          name: "State",
          type: "text",
          label: "State",
          placeHolder: "",
          validation: {
            required: false,
          },
          defaultError: "Please provide valid state",
          register,
          errors,
          defaultValue: profile.AltEmail || "",
        })}

        <Row>
          <Col md="12" lg="6" xl="4">
            {inputComponent({
              name: "Postal",
              type: "text",
              label: "Postal",
              placeHolder: "",
              validation: {
                required: false,
              },
              defaultError: "Please provide valid email",
              register,
              errors,
              defaultValue: profile.AltEmail || "",
            })}
          </Col>

          <Col md="12" lg="6" xl="4">
            {inputComponent({
              name: "CountryCode",
              type: "select",
              label: "Country",
              placeHolder: "",
              validation: {
                required: false,
              },
              defaultError: "Please provide valid country",
              register,
              errors,
              selectOptions: countries.map((c) => ({
                label: c.ListValue,
                value: c.ListKey,
              })),
              defaultValue: profile.Phone || "US",
            })}
          </Col>
          <Col md="12" lg="6" xl="4">
            <Form.Label>Phone</Form.Label>
            <div className={errors["Phone"] && "has-danger"}>
              <Controller
                control={control}
                name="Phone"
                country={"us"}
                defaultValue={false}
                rules={{ required: true }}
                render={(props) => (
                  <PhoneInput
                    country={"us"}
                    onChange={(phoneWithCode, country, e, formattedValue) => {
                      const phoneWithoutCode = phoneWithCode.replace(
                        new RegExp(`\^${country.dialCode}`),
                        ""
                      );
                      setPhone({
                        phoneWithoutCode,
                        phoneWithCode,
                        countryCode: `+${country.dialCode}`,
                      });
                      props.onChange(phoneWithoutCode);
                    }}
                  />
                )}
              />
              {errors["Phone"] && (
                <label className="error text-danger">Phone number is required</label>
              )}
            </div>
          </Col>
        </Row>
        {inputComponent({
          name: "Website",
          type: "text",
          label: "Website",
          validation: {
            required: false,
            pattern: websiteRule,
          },
          register,
          errors,
          defaultError: (
            <span>
              Please provide valid <b>Website</b>
            </span>
          ),
        })}
      </form>
    );
  }
);

Step.propTypes = {
  countries: PropTypes.array.isRequired,
  currencies: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  countries: state.misc.countries,
  currencies: state.misc.currencies,
});

export default connect(mapStateToProps, {}, null, { forwardRef: true })(Step);
