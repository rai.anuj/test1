import React, { useEffect, useState, useRef } from "react";
import { Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { setNotification } from "../../../store/actions/misc";
import { inputComponent } from "../../../components/Form/Input";
import ImageUpload from "../../../components/ImageUpload";
import { buildErrorMsg } from "../../../utils/error";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import { websiteRule, emailRule } from "../../../utils/validationRules";

const Step = React.forwardRef(({ afterValidate, countries }, ref) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook

  const [primaryPhone, setPrimaryPhone] = useState();
  const [primaryMobile, setPrimaryMobile] = useState();
  const [billingPhone, setBillingPhone] = useState();
  const [billingMobile, setBillingMobile] = useState();

  const onSubmit = (data) => {
    data.PrimaryPhoneCountryCode =
      (primaryPhone && primaryPhone.countryCode) || "";
    data.PrimaryPhone = (primaryPhone && primaryPhone.phoneWithoutCode) || "";

    data.PrimaryMobileCountryCode =
      (primaryMobile && primaryMobile.countryCode) || "";
    data.PrimaryMobile =
      (primaryMobile && primaryMobile.phoneWithoutCode) || "";

    data.BillingPhoneCountryCode =
      (billingPhone && billingPhone.countryCode) || "";
    data.BillingPhone = (billingPhone && billingPhone.phoneWithoutCode) || "";

    data.BillingMobileCountryCode =
      (billingMobile && billingMobile.countryCode) || "";
    data.BillingMobile =
      (billingMobile && billingMobile.phoneWithoutCode) || "";
    afterValidate(data, "contact");
  };
  const profile = {};
  return (
    <form ref={ref} onSubmit={handleSubmit(onSubmit)}>
      <h6 className="text-default">Primary Contact Details</h6>
      {inputComponent({
        name: "PrimaryContact",
        type: "text",
        label: "Contact Person",
        validation: {
          required: true,
        },
        defaultError: (
          <span>
            Please provide valid <b>Contact Name</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      {inputComponent({
        name: "PrimaryEmail",
        type: "text",
        label: "Email",
        validation: {
          required: true,
          pattern: emailRule,
        },
        defaultError: (
          <span>
            Please provide valid <b>primary cotact person's email</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      <Row>
        <Col md="12" lg="6" xl="4">
          <Form.Label>Phone</Form.Label>
          <PhoneInput
            country={"us"}
            value={primaryPhone && primaryPhone.phone}
            onChange={(phone, country, e, formattedValue) => {
              const phoneWithoutCode = phone.replace(
                new RegExp(`\^${country.dialCode}`),
                ""
              );
              setPrimaryPhone({
                phoneWithoutCode,
                phone,
                countryCode: `+${country.dialCode}`,
              });
            }}
          />
        </Col>
        <Col md="12" lg="6" xl="4">
          <Form.Label>Mobile</Form.Label>
          <PhoneInput
            country={"us"}
            value={primaryMobile && primaryMobile.phone}
            onChange={(phone, country, e, formattedValue) => {
              const phoneWithoutCode = phone.replace(
                new RegExp(`\^${country.dialCode}`),
                ""
              );
              setPrimaryMobile({
                phoneWithoutCode,
                phone,
                countryCode: `+${country.dialCode}`,
              });
            }}
          />
        </Col>
      </Row>
      <hr />
      <h6 className="text-default">Billing Contact Details</h6>
      {inputComponent({
        name: "BillingContact",
        type: "text",
        label: "Contact Person",
        validation: {
          required: true,
        },
        defaultError: (
          <span>
            Please provide valid <b>Contact Person Name</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      {inputComponent({
        name: "BillingEmail",
        type: "email",
        label: "Email",
        validation: {
          required: true,
          pattern: emailRule,
        },
        defaultError: (
          <span>
            Please provide valid <b>Email</b>
          </span>
        ),
        register,
        errors,
        defaultValue: profile.FirstName || "",
      })}
      <Row>
        <Col md="12" lg="6" xl="4">
          <Form.Label>Phone</Form.Label>
          <PhoneInput
            country={"us"}
            value={billingPhone && billingPhone.phone}
            onChange={(phone, country, e, formattedValue) => {
              const phoneWithoutCode = phone.replace(
                new RegExp(`\^${country.dialCode}`),
                ""
              );
              setBillingPhone({
                phoneWithoutCode,
                phone,
                countryCode: `+${country.dialCode}`,
              });
            }}
          />
        </Col>
        <Col md="12" lg="6" xl="4">
          <Form.Label>Mobile</Form.Label>
          <PhoneInput
            country={"us"}
            value={billingMobile && billingMobile.phone}
            onChange={(phone, country, e, formattedValue) => {
              const phoneWithoutCode = phone.replace(
                new RegExp(`\^${country.dialCode}`),
                ""
              );
              setBillingMobile({
                phoneWithoutCode,
                phone,
                countryCode: `+${country.dialCode}`,
              });
            }}
          />
        </Col>
      </Row>
    </form>
  );
});

Step.propTypes = {
  countries: PropTypes.array.isRequired,
  currencies: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  countries: state.misc.countries,
  currencies: state.misc.currencies,
});

export default connect(mapStateToProps, {}, null, { forwardRef: true })(Step);
