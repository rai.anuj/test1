import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Row, Col, Tab } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Table from "./Table";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import SweetAlert from "react-bootstrap-sweetalert";
import { getTickets } from "../../../store/actions/tickets";
import Add from "./Add";
import { setNotification } from "../../../store/actions/misc";
const Index = ({ tickets, getTickets, setNotification }) => {
  const [addShow, setAddShow] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [selMemeber, setSelMemeber] = useState();

  useEffect(() => {
    getTickets();
  }, []);
  return (
    <div>
      <Table
        data={tickets}
        onDelete={() => setDeleteConfirm(true)}
        onEdit={(obj) => {
          setSelMemeber(obj);
          setAddShow(true);
        }}
        onAddClick={() => {
          setSelMemeber();
          setAddShow(true);
        }}
      />

      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => setDeleteConfirm(false)}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
      <Add
        setAddShow={setAddShow}
        addShow={addShow}
        selMemeber={selMemeber}
        onData={(data) => {
          setAddShow(false);
          setNotification({
            message: "Ticket issued successfully!",
          });
        }}
      />
    </div>
  );
};

Index.propTypes = {
  getTickets: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  tickets: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  tickets: state.tickets.list,
});

export default connect(mapStateToProps, { getTickets, setNotification })(Index);
