import React from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Add = ({ addShow, setAddShow, selMemeber, onData }) => {
  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>{selMemeber ? "Edit" : "Issue"} Ticket</Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Search email"
              defaultValue={selMemeber && selMemeber.email}
            />
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="email"
              placeholder="Name"
              defaultValue={
                selMemeber && selMemeber.name.first + " " + selMemeber.name.last
              }
            />
          </Form.Group>
          <Form.Group controlId="ControlSelect1">
            <Form.Label>Price Type</Form.Label>
            <Form.Control
              as="select"
              defaultValue={selMemeber && selMemeber.membershipType}
            >
              <option>Life Member</option>
              <option>Annual Member</option>
              <option>ANon Member</option>
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label># Views</Form.Label>
            <Form.Control
              type="number"
              placeholder="Allowed Views"
              defaultValue={selMemeber && selMemeber.views}
            />
          </Form.Group>
        </Modal.Body>
      </Form>
      <Modal.Footer className="pl4 pr-4">
        <Button
          className="btn-round mr-2"
          variant="default"
          onClick={() => setAddShow(false)}
        >
          Cancel
        </Button>
        <Button
          className="btn-round"
          variant="info"
          onClick={() => onData(false)}
        >
          Issue Ticket
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Add;
