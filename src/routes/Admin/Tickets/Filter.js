import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Card, Row, Col, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactDatetime from "react-datetime";

const Filter = () => {
  return (
    <Row>
      <Col sm="12" md="6">
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td style={{ width: "100px" }}>Collection</td>
              <td className="p-2">
                <Form.Control as="select">
                  <option value=""></option>
                  <option>Collection 1</option>
                  <option>Collection 1</option>
                  <option>Collection 2</option>
                  <option>Collection 3</option>
                </Form.Control>
              </td>
            </tr>
            <tr>
              <td>Price List</td>
              <td className="p-2">
                <Form.Control as="select">
                  <option value=""></option>
                  <option>Life Member</option>
                  <option>Annual Member</option>
                  <option>Non Member</option>
                </Form.Control>
              </td>
            </tr>
          </tbody>
        </table>
      </Col>
      <Col sm="12" md="6">
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              <td style={{ width: "100px" }}>Videos</td>
              <td className="p-2">
                <Form.Control as="select">
                  <option value=""></option>
                  <option>Video #1</option>
                  <option>Video #2</option>
                  <option>Video #3</option>
                </Form.Control>
              </td>
            </tr>
            <tr>
              <td>Dates</td>
              <td className="p-2">
                <Row>
                  <Col xs="6">
                    <ReactDatetime
                      inputProps={{
                        className: "form-control",
                        placeholder: "From",
                      }}
                      timeFormat={false}
                    />
                  </Col>
                  <Col xs="6">
                    <ReactDatetime
                      inputProps={{
                        className: "form-control",
                        placeholder: "To",
                      }}
                      timeFormat={false}
                    />
                  </Col>
                </Row>
              </td>
            </tr>
          </tbody>
        </table>
      </Col>
      <Col sm="12" md="6">
        <Button variant="default" size="sm">
          Filter
        </Button>
      </Col>
    </Row>
  );
};

export default Filter;
