import React from "react";

// reactstrap components
import { Button, Card, Row, Col, Container } from "react-bootstrap";

// core components
import ReactTable from "../../../components/Table/ReactTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class ReactTables extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.data.map((prop, key) => {
      return {
        id: prop._id,
        name: `${prop.name.first} ${prop.name.last}`,
        email: prop.email,
        status: prop.status,
        actions: (
          // we've added some custom button actions
          <div className="actions-right">
            <Button
              onClick={() => {
                const obj = this.props.data.find((o) => o._id === prop._id);
                this.props.onEdit(obj);
              }}
              variant="info"
              size="sm"
              className="btn-icon btn-link edit"
            >
              <FontAwesomeIcon icon={["fas", "edit"]} />
            </Button>
            <Button
              onClick={() => {
                const obj = this.props.data.find((o) => o.id === prop._id);
                this.props.onDelete(obj);
              }}
              variant="danger"
              size="sm"
              className="btn-icon btn-link remove"
            >
              <FontAwesomeIcon icon={["fas", "trash"]} />
            </Button>
          </div>
        ),
      };
    });
    const membershipType = [
      ...new Set(data.map((d) => d.membershipType)),
    ].sort();
    const status = [...new Set(data.map((d) => d.status))].sort();
    return (
      <>
        <Container fluid>
          <Row>
            <Col md="12">
              <div className="clearfix">
                <div className="float-right">
                  <Button
                    variant="success"
                    size="sm"
                    onClick={this.props.onAddClick}
                  >
                    <FontAwesomeIcon icon={["fas", "plus"]} /> Add
                  </Button>
                </div>
              </div>
            </Col>
            <Col md="12">
              <Card className="responsive">
                <Card.Header>
                  <Card.Title tag="h4">Admin Users</Card.Title>
                  <hr />
                </Card.Header>
                <Card.Body>
                  <ReactTable
                    hasGlobalFilter
                    data={data}
                    columns={[
                      {
                        Header: "Name",
                        accessor: "name",
                        filterable: true,
                        filterConfig: {
                          type: "text",
                        },
                      },
                      {
                        Header: "Email",
                        accessor: "email",
                        filterable: true,
                        filterConfig: {
                          type: "text",
                        },
                      },
                      {
                        Header: "Actions",
                        accessor: "actions",
                        sortable: false,
                        filterable: false,
                        actionColumn: true,
                        width: 140,
                      },
                    ]}
                    className="-striped -highlight primary-pagination"
                  />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default ReactTables;
