import React from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Add = ({ addShow, setAddShow, selMemeber }) => {
  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>{selMemeber ? "Edit" : "Add"} Admin User</Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              defaultValue={selMemeber && selMemeber.email}
            />
          </Form.Group>
          <Form.Group controlId="formFirstName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              defaultValue={
                selMemeber && selMemeber.name.first + " " + selMemeber.name.last
              }
            />
          </Form.Group>
        </Modal.Body>
      </Form>
      <Modal.Footer className="pl4 pr-4">
        <Button
          className="btn-round mr-2"
          variant="default"
          onClick={() => setAddShow(false)}
        >
          Cancel
        </Button>
        <Button
          className="btn-round"
          variant="info"
          onClick={() => setAddShow(false)}
        >
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Add;
