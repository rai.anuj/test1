import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import Add from "./Add";
import SweetAlert from "react-bootstrap-sweetalert";
import RT from "./ReactTable";
import { getMembers } from "../../../store/actions/members";

const Members = (props) => {
  useEffect(() => {
    props.getMembers();
  }, []);
  useEffect(() => {
    setAdmins(
      props.members.splice(Math.floor(Math.random() * props.members.length), 5)
    );
  }, [props.members]);
  const [admins, setAdmins] = useState([]);
  const [addShow, setAddShow] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [selMemeber, setSelMemeber] = useState();
  return (
    <div>
      <RT
        data={admins}
        onDelete={() => setDeleteConfirm(true)}
        onEdit={(obj) => {
          setSelMemeber(obj);
          setAddShow(true);
        }}
        onAddClick={() => {
          setSelMemeber();
          setAddShow(true);
        }}
      />
      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => setDeleteConfirm(false)}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
      <Add setAddShow={setAddShow} addShow={addShow} selMemeber={selMemeber} />
    </div>
  );
};

Members.propTypes = {
  getMembers: PropTypes.func.isRequired,
  members: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  members: state.members.list,
});

export default connect(mapStateToProps, { getMembers })(Members);
