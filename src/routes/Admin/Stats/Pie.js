import React from "react";
import { Pie } from "react-chartjs-2";

const data = {
  labels: ["Red", "Blue", "Yellow"],
  datasets: [
    {
      data: [300, 50, 100],
      backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
      hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
    },
  ],
};


const options = {
    maintainAspectRatio: false,
    responsive: true,

    layout: {
      padding: { left: 0, right: 0, top: 15, bottom: 15 },
    },
  };
export default class Chart extends React.Component {
  render() {
    return <Pie data={data} options={options} />;
  }
}
