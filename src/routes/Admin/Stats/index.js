import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Line from "./Line";
import Pie from "./Pie";
import Mix from "./Mix";

const Stats = () => {
  const [addShow, setAddShow] = useState(false);

  return (
    <div className="p-4 m-2">
      <Row>
        <Col sm="12">
          <Card style={{ background: "white" }} className="mb-4">
            <Card.Header>
              <Card.Title>Views</Card.Title>
            </Card.Header>
            <Card.Body>
              <div style={{ height: "300px" }}>
                <Line />
              </div>
            </Card.Body>
          </Card>
        </Col>
        <Col sm="6">
          <Card style={{ background: "white" }} className="mb-4">
            <Card.Header>
              <Card.Title>Collection Revenue</Card.Title>
            </Card.Header>
            <Card.Body>
              <div style={{ height: "300px" }}>
                <Pie />
              </div>
            </Card.Body>
          </Card>
        </Col>
        <Col sm="6">
          <Card style={{ background: "white" }} className="mb-4">
            <Card.Header>
              <Card.Title>Sales</Card.Title>
            </Card.Header>
            <Card.Body>
              <div style={{ height: "300px" }}>
                <Mix />
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Stats;
