import React from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { buildErrorMsg } from "../../../utils/error";
import { useForm } from "react-hook-form";
import { inputComponent } from "../../../components/Form/Input";
import { emailRule } from "../../../utils/validationRules";
import { memberStatus } from "../../../utils";

const Add = ({
  addShow,
  setAddShow,
  selData,
  onSubmit,
  actionError,
  memberTypes,
}) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook

  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Modal.Header closeButton>
          <Modal.Title>
            {Object.keys(selData).length > 0 ? "Edit" : "Add"} Member
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {actionError && (
            <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
          )}
          {inputComponent({
            name: "FullName",
            type: "text",
            label: "Full Name",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Full Name</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.FullName || "",
          })}
          {inputComponent({
            name: "MemberTypeId",
            type: "select",
            label: "Member Type",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Membership Type</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.MemberTypeId || "",
            selectOptions: memberTypes.map((m) => ({
              label: m.Name,
              value: m.Id,
            })),
          })}
          {inputComponent({
            name: "Email",
            type: "email",
            label: "Email",
            validation: {
              required: true,
              pattern: emailRule,
            },
            defaultError: (
              <span>
                Please provide valid <b>Email</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.Email || "",
          })}
          {inputComponent({
            name: "MemberStatus",
            type: "select",
            label: "Member Status",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Membership Status</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.MemberStatus || "",
            selectOptions: memberStatus,
          })}
        </Modal.Body>

        <Modal.Footer className="pl4 pr-4">
          <Button
            className="btn-round mr-2"
            variant="default"
            onClick={() => setAddShow(false)}
          >
            Cancel
          </Button>
          <Button type="submit" className="btn-round" variant="info">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default Add;
