import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import Add from "./Add";
import SweetAlert from "react-bootstrap-sweetalert";
import RT from "./ReactTable";
import { setNotification } from "../../../store/actions/misc";
import {
  memberAdd,
  memberUpdate,
  memberDelete,
} from "../../../store/actions/admin";

const Members = (props) => {
  const [addShow, setAddShow] = useState(false);
  const [deleteConfirm, setDeleteConfirm] = useState(false);
  const [selData, setSelData] = useState({});

  useEffect(() => {
    setAddShow(false);
    setSelData({});
  }, [props.addSuccess, props.updateSuccess]);

  return (
    <div>
      <RT
        data={props.members}
        onDelete={(obj) => {
          setSelData(obj);
          setDeleteConfirm(true);
        }}
        onEdit={(obj) => {
          setSelData(obj);
          setAddShow(true);
        }}
        onAddClick={() => {
          setSelData({});
          setAddShow(true);
        }}
      />
      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => {
          props.memberDelete(props.tenant.TenantCode, selData.Id);
          setDeleteConfirm(false);
        }}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
      <Add
        actionError={props.addError || props.updateError}
        setAddShow={setAddShow}
        addShow={addShow}
        selData={selData}
        memberTypes={props.memberTypes}
        onSubmit={(data) => {
          if (props.tenant.TenantCode) {
            if (selData && selData.Id) {
              data.Id = selData.Id;
              data.RowVersion = selData.RowVersion;
              props.memberUpdate(props.tenant.TenantCode, selData.Id, data);
            } else {
              props.memberAdd(props.tenant.TenantCode, data);
            }
          }
        }}
      />
    </div>
  );
};

Members.propTypes = {
  setNotification: PropTypes.func.isRequired,
  members: PropTypes.array.isRequired,
  memberTypes: PropTypes.array.isRequired,
  memberAdd: PropTypes.func.isRequired,
  memberUpdate: PropTypes.func.isRequired,
  memberDelete: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  members: state.admin.members,
  memberTypes: state.admin.memberTypes,
  addError: state.admin.addError,
  addSuccess: state.admin.addSuccess,
  updateError: state.admin.updateError,
  updateSuccess: state.admin.updateSuccess,
  deleteError: state.admin.deleteError,
  deleteSuccess: state.admin.deleteSuccess,
});

export default connect(mapStateToProps, {
  setNotification,
  memberUpdate,
  memberAdd,
  memberDelete,
})(Members);
