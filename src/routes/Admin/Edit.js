import React, { useState, useEffect } from "react";
import { Button, Jumbotron, Tabs, Tab, Card, Image } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import Members from "./Members";
import Videos from "./Videos";
import Collections from "./Collections";
import Tickets from "./Tickets";
import Stats from "./Stats";
import MemberTypes from "./MemberTypes";
import AdminUsers from "./AdminUsers";
import { getChannelImage, getSocial } from "../../utils/images";
import { setNotification } from "../../store/actions/misc";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import VisibilitySensor from "react-visibility-sensor";

import { getAccessibleChannels } from "../../store/actions/channels";
import {
  getMemberTypeList,
  getMemberList,
  getVideoList,
  getCollectionList,
  getCollectionVideoList,
} from "../../store/actions/admin";

import { getAccessibleCollections } from "../../store/actions/collections";

import "./index.css";
import ManageChannelProfile from "./ManageChannelProfile";

const AdminHomePage = (props) => {
  const [showLeft, setShowLeft] = useState(false);
  const [showRight, setShowRight] = useState(false);
  const [addShowChannelDetails, setAddShowChannelDetails] = useState(false);

  const tenant = props.tenants.find((t) => t.TenantId == props.match.params.id);
  useEffect(() => {
    const tenant = props.tenants.find(
      (t) => t.TenantId == props.match.params.id
    );
    if (tenant && tenant.TenantCode) {
      props.getMemberTypeList(tenant.TenantCode);
      props.getMemberList(tenant.TenantCode);
      props.getVideoList(tenant.TenantCode);
      props.getCollectionList(tenant.TenantCode);
      props.getCollectionVideoList(tenant.TenantCode);
    }
  }, [props.tenants]);

  useEffect(() => {
    if (props.addSuccess) {
      props.setNotification({ message: props.addSuccess });
    }
  }, [props.addSuccess]);
  useEffect(() => {
    if (props.updateSuccess) {
      props.setNotification({ message: props.updateSuccess });
    }
  }, [props.updateSuccess]);
  useEffect(() => {
    if (props.deleteSuccess) {
      props.setNotification({ message: props.deleteSuccess });
    }
  }, [props.deleteSuccess]);
  useEffect(() => {
    if (props.deleteError) {
      props.setNotification({
        message:
          typeof props.deleteError == "string"
            ? props.deleteError
            : "Error occured while deleting",
      });
    }
  }, [props.deleteError]);
  useEffect(() => {
    if (!Array.isArray(props.collections[props.match.params.id])) {
      props.getAccessibleCollections(props.match.params.id);
    }
  }, []);

  const onLeftTabVisibiltyChange = (isVisible) => {
    setShowLeft(!isVisible);
  };

  const onRightTabVisibiltyChange = (isVisible) => {
    setShowRight(!isVisible);
  };

  const channel = props.channels.find((c) => c.id == props.match.params.id);
  let bg;
  let logo;
  if (channel) {
    bg = getChannelImage(channel.images, "banner");
    logo = getChannelImage(channel.images, "logo");
  }
  //setAddShowChannelDetails
  return (
    <div className="m-md-1 m-xl-4 m-1">
      {channel ? (
        <React.Fragment>
          <div>
            {bg && (
              <div
                className="channel-banner-img"
                style={{ backgroundImage: `url(${bg})` }}
              ></div>
            )}
            <Jumbotron style={{ padding: "1rem 1rem 0.5rem 1rem" }}>
              <div>
                {logo && (
                  <div className="channel-page-logo-container">
                    <Image fluid src={logo} />
                  </div>
                )}
                <div
                  className={`channel-page-detail-container ${
                    logo && "pl-0 pl-lg-4"
                  }`}
                >
                  <div className="name">{channel.orgname}</div>
                  <div className="address">{channel.address}</div>
                  <p className="pt-2 text-default">{channel.descr}</p>
                </div>
              </div>
              <hr style={{ marginTop: "0.5rem", marginBottom: "0.5rem" }} />
              <div className="clearfix">
                <div className="social float-md-left">{getSocial(channel)}</div>
                <div className="float-md-right pt-0">
                  <Button size="sm" className="mt-0 mb-0">
                    <FontAwesomeIcon icon={["fas", "edit"]} /> Edit
                  </Button>
                </div>
              </div>
            </Jumbotron>
          </div>
        </React.Fragment>
      ) : null}
      <div className="pt-4">
        <Card className="responsive">
          <div className="nav-tab-admin1-container">
            <div
              className={`l-arrow-container ${showLeft ? "d-block" : "d-none"}`}
            >
              <div
                className="l-arrow"
                onClick={() => {
                  let el = document.getElementsByClassName("nav-tab-admin1")[0];
                  el.scrollTo({
                    top: 0,
                    left: el.scrollLeft - 75,
                    behavior: "smooth",
                  });
                }}
              >
                <FontAwesomeIcon icon={["fas", "angle-left"]} />
              </div>
            </div>
            <Tabs
              defaultActiveKey="members"
              className="ml-4 mr-4 mt-4 mb-0 nav-tab-admin1"
            >
              <Tab
                eventKey="members"
                title={
                  <span>
                    <VisibilitySensor onChange={onLeftTabVisibiltyChange}>
                      <span>&nbsp;</span>
                    </VisibilitySensor>
                    Members
                  </span>
                }
              >
                <Members tenant={tenant} />
              </Tab>
              <Tab eventKey="videos" title="Videos">
                <Videos tenant={tenant} />
              </Tab>
              <Tab eventKey="collections" title="Collections & Price">
                <Collections tenant={tenant} />
              </Tab>
              <Tab eventKey="tickets" title="Tickets">
                <Tickets tenant={tenant} />
              </Tab>
              <Tab eventKey="statistics" title="Statistics">
                <Stats tenant={tenant} />
              </Tab>
              <Tab eventKey="memtypes" title="Member Types">
                <MemberTypes tenant={tenant} />
              </Tab>
              <Tab
                eventKey="admin_users"
                title={
                  <span>
                    Admin Users
                    <VisibilitySensor onChange={onRightTabVisibiltyChange}>
                      <span>&nbsp;</span>
                    </VisibilitySensor>
                  </span>
                }
              >
                <AdminUsers />
              </Tab>
            </Tabs>
            <div
              className={`r-arrow-container ${
                showRight ? "d-block" : "d-none"
              }`}
            >
              <div
                className="r-arrow"
                onClick={() => {
                  let el = document.getElementsByClassName("nav-tab-admin1")[0];
                  el.scrollTo({
                    top: 0,
                    left: el.scrollLeft + 75,
                    behavior: "smooth",
                  });
                }}
              >
                <FontAwesomeIcon icon={["fas", "angle-right"]} />
              </div>
            </div>
          </div>
        </Card>
      </div>
      <ManageChannelProfile
        setAddShow={setAddShowChannelDetails}
        addShow={addShowChannelDetails}
        type={"Edit"}
      />
    </div>
  );
};

AdminHomePage.propTypes = {
  getMemberTypeList: PropTypes.func.isRequired,
  getMemberList: PropTypes.func.isRequired,
  getVideoList: PropTypes.func.isRequired,
  getCollectionList: PropTypes.func.isRequired,
  getCollectionVideoList: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  collections: PropTypes.object.isRequired,
  getAccessibleChannels: PropTypes.func.isRequired,
  channels: PropTypes.array.isRequired,
  getAccessibleCollections: PropTypes.func.isRequired,
  tenants: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  purchase: state.purchase,
  channels: state.channels.list,
  collections: state.collections.collections,
  tenants: state.channels.tenants,

  addError: state.admin.addError,
  addSuccess: state.admin.addSuccess,
  updateError: state.admin.updateError,
  updateSuccess: state.admin.updateSuccess,
  deleteError: state.admin.deleteError,
  deleteSuccess: state.admin.deleteSuccess,
});

export default withRouter(
  connect(mapStateToProps, {
    getAccessibleChannels,
    getAccessibleCollections,
    setNotification,
    getMemberTypeList,
    getMemberList,
    getVideoList,
    getCollectionList,
    getCollectionVideoList,
  })(AdminHomePage)
);
