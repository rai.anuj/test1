import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
} from "react-bootstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ChannelBox from "../../components/Channel/ChannelBox";
import ManageChannelProfile from "./ManageChannelProfile";
import { getChannelImage } from "../../utils/images";

import {
  getAuthorizedTenants,
  getAccessibleChannels,
} from "../../store/actions/channels";

const Index = ({
  tenants,
  channels,
  getAuthorizedTenants,
  getAccessibleChannels,
  username,
}) => {
  const [addShowChannelDetails, setAddShowChannelDetails] = useState(false);
  useEffect(() => {
    if (channels.length == 0) {
      getAccessibleChannels();
    }
  }, []);
  const aChannels =
    (username &&
      channels.filter(
        (c) =>
          c.userorgrel.findIndex((p) => p.username == username && p.admin) >= 0
      )) ||
    [];

  return (
    <div className="m-md-1 m-xl-4 m-1">
      <Card className="mb-4 figure-fluid">
        <Card.Header>
          <div className="clearfix">
            <Card.Title className="float-left">Manage Channels</Card.Title>
            <Button
              as={Link}
              className="float-right"
              size="sm"
              variant="info"
              to="/admin/create-channel"
            >
              <i className="fa fa-plus" /> Create Channel
            </Button>
          </div>
          <hr />
        </Card.Header>
        <Card.Body>
          <Row>
            {aChannels.map((v) => (
              <ChannelBox
                key={v.id}
                url={`/admin/${v.id}`}
                logo={getChannelImage(v.images, "logo")}
                title={v.orgname}
              />
            ))}
          </Row>
        </Card.Body>
      </Card>
      <ManageChannelProfile
        setAddShow={setAddShowChannelDetails}
        addShow={addShowChannelDetails}
        type={"Edit"}
      />
    </div>
  );
};

Index.propTypes = {
  getAuthorizedTenants: PropTypes.func.isRequired,
  getAccessibleChannels: PropTypes.func.isRequired,
  tenants: PropTypes.array.isRequired,
  channels: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  tenants: state.channels.tenants,
  channels: state.channels.list,
  username: state.user.profile.username,
});

export default connect(mapStateToProps, {
  getAuthorizedTenants,
  getAccessibleChannels,
})(Index);
