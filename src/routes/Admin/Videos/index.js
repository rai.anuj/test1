import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Card, Row, Col, Figure } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import SweetAlert from "react-bootstrap-sweetalert";

import Add from "./Add";
import "./index.css";
import {
  videoAdd,
  videoUpdate,
  videoDelete,
} from "../../../store/actions/admin";
import defaultImg from "../../../assets/images/app-logo.png";

const ImgLogoContainer = styled.div`
  height: 300px;

  &:before {
    content: "";
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }

  img {
    max-height: 300px;
  }
`;
const Videos = (props) => {
  const [addShow, setAddShow] = useState(false);
  const [selData, setSelData] = useState({});
  const [deleteConfirm, setDeleteConfirm] = useState(false);

  useEffect(() => {
    setAddShow(false);
    setSelData({});
  }, [props.addSuccess, props.updateSuccess]);
  return (
    <div className="container-fluid figure-fluid">
      <div>
        <div className="float-right">
          <Button
            variant="success"
            size="sm"
            onClick={() => {
              setSelData({});
              setAddShow(true);
            }}
          >
            <FontAwesomeIcon icon={["fas", "plus"]} /> Add
          </Button>
        </div>
      </div>
      <div className="clearfix">
        <Add
          setAddShow={setAddShow}
          addShow={addShow}
          actionError={props.addError || props.updateError}
          selData={selData}
          memberTypes={props.memberTypes}
          onDelete={() => {
            setDeleteConfirm(true);
            setAddShow(false);
          }}
          onSubmit={(data) => {
            data.VideoStatus = 5;
            if (props.tenant.TenantCode) {
              if (selData && selData.Id) {
                data.Id = selData.Id;
                data.RowVersion = selData.RowVersion;
                props.videoUpdate(props.tenant.TenantCode, selData.Id, data);
              } else {
                props.videoAdd(props.tenant.TenantCode, data);
              }
            }
          }}
        />
      </div>
      <Card className="responsive">
        <Card.Header>
          <Card.Title tag="h4">Videos</Card.Title>
          <hr />
        </Card.Header>
        <Card.Body>
          <Row className="figure-fluid">
            {props.videos.map((v, k) => (
              <Col
                xs={12}
                sm={6}
                md={4}
                lg={3}
                className="p-4 cursor-pointer"
                onClick={() => console.log(true)}
                key={k}
              >
                <Card
                  style={{ height: "100%" }}
                  onClick={() => {
                    setAddShow(true);
                    setSelData(v);
                  }}
                >
                  <Card.Body>
                    <Figure>
                      <ImgLogoContainer>
                        <Figure.Image
                          className="p-4"
                          alt={v.VideoTitle}
                          src={defaultImg}
                        />
                      </ImgLogoContainer>
                      <hr />
                      <div>
                        <div>
                          <Figure.Caption style={{ color: "#000" }}>
                            {v.VideoTitle}
                          </Figure.Caption>
                        </div>
                      </div>
                      <div
                        className="text-center text-default"
                        style={{ fontSize: "12px" }}
                      >
                        {v.VideoDescr}
                      </div>
                    </Figure>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Card.Body>
      </Card>
      <SweetAlert
        warning
        showCancel
        confirmBtnText="Yes, delete it!"
        confirmBtnBsStyle="danger"
        title="Are you sure?"
        onConfirm={() => {
          props.videoDelete(props.tenant.TenantCode, selData.Id);
          setDeleteConfirm(false);
        }}
        onCancel={() => setDeleteConfirm(false)}
        focusCancelBtn
        show={deleteConfirm}
      >
        You will not be able to recover this data!
      </SweetAlert>
    </div>
  );
};

Videos.propTypes = {
  videos: PropTypes.array.isRequired,
  videoAdd: PropTypes.func.isRequired,
  videoUpdate: PropTypes.func.isRequired,
  videoDelete: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  videos: state.admin.videos,
  addError: state.admin.addError,
  addSuccess: state.admin.addSuccess,
  updateError: state.admin.updateError,
  updateSuccess: state.admin.updateSuccess,
  deleteError: state.admin.deleteError,
  deleteSuccess: state.admin.deleteSuccess,
});

export default connect(mapStateToProps, {
  videoDelete,
  videoUpdate,
  videoAdd,
})(Videos);
