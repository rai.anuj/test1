import React, { useState, useEffect } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SimpleFileButton from "../../../components/FileUpload/SimpleFileButton";
import Select from "react-select";

import { buildErrorMsg } from "../../../utils/error";
import { useForm } from "react-hook-form";
import { inputComponent } from "../../../components/Form/Input";
import { videoSourceRegEx } from "../../../utils/validationRules";
import { videoSource, rowStatus, getStatus } from "../../../utils";

const Add = ({
  addShow,
  setAddShow,
  selData,
  onSubmit,
  actionError,
  onDelete,
}) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const [videoSourceSel, setVideoSourceSel] = useState("YouTube");
  useEffect(() => {
    if (selData.VideoSource) {
      setVideoSourceSel(getStatus(videoSource, selData.VideoSource));
    }
  }, [selData]);
  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          {Object.keys(selData).length > 0 ? "Edit" : "Add"} Video
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Modal.Body>
          {actionError && (
            <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
          )}
          {inputComponent({
            name: "VideoTitle",
            type: "text",
            label: "VideoTitle",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>VideoTitle</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.VideoTitle || "",
          })}
          {inputComponent({
            name: "VideoDescr",
            type: "textarea",
            label: "VideoDescr",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>VideoDescr</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.VideoDescr || "",
          })}
          {inputComponent({
            name: "VideoSource",
            type: "select",
            label: "Video Source",
            validation: {
              required: true,
            },
            defaultError: (
              <span>
                Please provide valid <b>Video Source</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.VideoSource || "5",
            selectOptions: videoSource,
            inputProps: {
              onChange: (e) =>
                setVideoSourceSel(getStatus(videoSource, e.target.value)),
            },
          })}
          {inputComponent({
            name: "VideoId",
            type: "text",
            label: "Video Id",
            placeholder: "Valid video ID",
            validation: {
              required: true,
              pattern: videoSourceRegEx[videoSourceSel],
            },
            defaultError: (
              <span>
                Please provide valid <b>Video ID</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.VideoId || "",
          })}
          {inputComponent({
            name: "TrailerVideoId",
            type: "text",
            label: "Trailer Video Id",
            placeholder: "Valid video ID from youtube",
            validation: {
              required: true,
              pattern: videoSourceRegEx[videoSourceSel],
            },
            defaultError: (
              <span>
                Please provide valid <b>Trailer Video ID</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.TrailerVideoId || "",
          })}
          {inputComponent({
            name: "Artists",
            type: "textarea",
            label: "Artists",
            validation: {
              required: false,
            },
            defaultError: (
              <span>
                Please provide valid <b>Artists</b>
              </span>
            ),
            register,
            errors,
            defaultValue: selData.Artists || "",
          })}
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Status</Form.Label>
            <div>
              <div>
                <span className="form-check-radio d-inline-block pr-4">
                  <label className="form-check-label">
                    <input
                      name="VideoStatus"
                      type="radio"
                      className="form-check-input"
                      checked
                    />
                    Draft <span className="form-check-sign"></span>
                  </label>
                </span>
                <span className="form-check-radio d-inline-block pr-4">
                  <label className="form-check-label">
                    <input
                      name="VideoStatus"
                      type="radio"
                      className="form-check-input"
                    />
                    Active <span className="form-check-sign"></span>
                  </label>
                </span>
                <span className="form-check-radio d-inline-block pr-4">
                  <label className="form-check-label">
                    <input
                      name="VideoStatus"
                      type="radio"
                      className="form-check-input"
                    />
                    Inactive <span className="form-check-sign"></span>
                  </label>
                </span>
              </div>
            </div>
          </Form.Group>

          <div>
            <Form.Group className="file-select-4 d-inline-block">
              <SimpleFileButton
                label="Upload Cover Image"
                btnclassName="default"
                className="mr-1"
              />
            </Form.Group>
          </div>
        </Modal.Body>

        <Modal.Footer className="pl4 pr-4">
          {Object.keys(selData).length > 0 && (
            <Button
              className="btn-round mr-2"
              variant="danger"
              onClick={onDelete}
            >
              Delete
            </Button>
          )}
          <Button
            className="btn-round mr-2"
            variant="default"
            onClick={() => setAddShow(false)}
          >
            Cancel
          </Button>
          <Button type="submit" className="btn-round" variant="info">
            Save
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default Add;
