import React from "react";
import { Modal, Button, Form, Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SimpleFileButton from "../../components/FileUpload/SimpleFileButton";

import { useForm } from "react-hook-form";
import { inputComponent } from "../../components/Form/Input";

import { buildErrorMsg } from "../../utils/error";

const ChannelDetails = ({ addShow, setAddShow, selMemeber, type }) => {
  return (
    <Modal size="lg" show={addShow} onHide={() => setAddShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>{type} Channel Detail</Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Channel Details</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter channel name"
              defaultValue={selMemeber && selMemeber.name.first}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              placeholder="Enter Description"
              row="5"
            />
          </Form.Group>
          <Form.Group className="file-select-4 d-inline-block">
            <SimpleFileButton
              label="Upload Channel Logo"
              btnclassName="default"
              className="mr-1"
            />
          </Form.Group>
          <Form.Group className="file-select-4 d-inline-block">
            <SimpleFileButton
              label="Upload Channel Banner"
              btnclassName="default"
              className="mr-1"
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Social Links</Form.Label>
            <Form.Control type="text" placeholder="Twitter" className="mb-4" />
            <Form.Control type="text" placeholder="Facebook" className="mb-4" />
            <Form.Control
              type="text"
              placeholder="Instagram"
              className="mb-4"
            />
          </Form.Group>
          <Form.Group className="form-check">
            <label className="form-check-label">
              <input type="checkbox" className="form-check-input" />
              Is Active? <span className="form-check-sign"></span>
            </label>
          </Form.Group>
        </Modal.Body>
      </Form>
      <Modal.Footer className="pl4 pr-4">
        <Button
          className="btn-round mr-2"
          variant="default"
          onClick={() => setAddShow(false)}
        >
          Cancel
        </Button>
        <Button
          className="btn-round"
          variant="info"
          onClick={() => setAddShow(false)}
        >
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ChannelDetails;
