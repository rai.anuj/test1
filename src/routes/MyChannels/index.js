import React, { useState } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
} from "react-bootstrap";
import MyChannels from "./MyChannels";

const Index = (props) => {
  return (
    <div className="m-md-1 m-xl-4 m-1">
      <MyChannels />
    </div>
  );
};

export default Index;
