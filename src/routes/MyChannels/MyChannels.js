import React, { useState } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
} from "react-bootstrap";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import ChannelBox from "../../components/Channel/ChannelBox";

import ExampleImg from "../../assets/images/example-logo.png";

const MyChannels = (props) => {
  return (
    <Card className="mb-4 figure-fluid">
      <Card.Header>
        <Card.Title>My Channel</Card.Title>
        <hr />
      </Card.Header>
      <Card.Body>
        <Row>
          {Array(5)
            .fill(1)
            .map((v, k) => (
              <ChannelBox
                key={k}
                url={`/channels/channel-${k + 1}`}
                img={ExampleImg}
                title={`Channel ${k + 56}`}
              />
            ))}
        </Row>
      </Card.Body>
    </Card>
  );
};

MyChannels.propTypes = {};

const mapStateToProps = (state) => ({});

export default withRouter(connect(mapStateToProps, {})(MyChannels));
