import React, { useEffect, useState, useRef } from "react";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { setNotification } from "../../store/actions/misc";
import { inputComponent } from "../../components/Form/Input";
import {
  saveUserProfile,
  saveUserProfilePhoto,
  deleteUserProfilePhoto,
  doChangePassword,
} from "../../store/actions/user";
import ImageUpload from "../../components/ImageUpload";
import { buildErrorMsg } from "../../utils/error";

const ChangePasswordForm = ({ doSave, actionError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const form = useRef();

  const onSubmit = (data, e) => {
    doSave(data);
    e.target.reset();
  };

  return (
    <form ref={form} onSubmit={handleSubmit(onSubmit)}>
      <Card.Body>
        {actionError && (
          <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
        )}
        {inputComponent({
          name: "oldPassword",
          type: "Password",
          label: "Old Password",
          validation: {
            required: { value: true, message: "Password is required" },
            pattern: {
              value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,30}$/,
              message:
                "Password must me atleast 6 characters, at least one uppercase letter, one lowercase letter, one number and one special character(@$!%)",
            },
          },
          register,
          errors,
        })}
        {inputComponent({
          name: "newPassword",
          type: "password",
          label: "New Password",
          validation: {
            required: { value: true, message: "New Password is required" },
            pattern: {
              value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,30}$/,
              message:
                "Password must me atleast 6 characters, at least one uppercase letter, one lowercase letter, one number and one special character(@$!%)",
            },
          },
          register,
          errors,
        })}
        {inputComponent({
          name: "confirmPassword",
          type: "password",
          label: "Confirm Password",
          placeHolder: "",
          validation: {
            required: { value: true, message: "Confirm Password is required" },
            minLength: {
              value: 6,
              message: "Confirm Password minimum length is 6",
            },
            validate: (value) =>
              value === watch("newPassword") ||
              "Confirm password doesn't match",
          },
          register,
          errors,
        })}
      </Card.Body>
      <Card.Footer>
        <Button className="btn-round mb-3" variant="success" type="submit">
          Update
        </Button>
      </Card.Footer>
    </form>
  );
};

const ProfileForm = ({ profile, doSave, actionError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    data.Id = profile.Id;
    doSave(data);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card.Body>
        {actionError && (
          <div className="text-danger pb-4">{buildErrorMsg(actionError)}</div>
        )}
        {inputComponent({
          name: "FirstName",
          type: "text",
          label: "First Name",
          validation: {
            required: true,
            pattern: /^[A-Za-z]+$/,
          },
          defaultError: "Please provide valid First Name",
          register,
          errors,
          defaultValue: profile.FirstName || "",
        })}
        {inputComponent({
          name: "LastName",
          type: "text",
          label: "Last Name",
          validation: {
            required: true,
            pattern: /^[A-Za-z]+$/,
          },
          defaultError: "Please provide Last Name",
          register,
          errors,
          defaultValue: profile.LastName || "",
        })}
        {inputComponent({
          name: "Phone",
          type: "text",
          label: "Phone",
          placeHolder: "",
          validation: {
            required: false,
            pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g,
          },
          defaultError: "Please provide valid phone",
          register,
          errors,
          defaultValue: profile.Phone || "",
        })}
        {inputComponent({
          name: "AltEmail",
          type: "email",
          label: "Alternate Email",
          placeHolder: "",
          validation: {
            required: false,
            pattern: /\S+@\S+\.\S+/,
          },
          defaultError: "Please provide valid email",
          register,
          errors,
          defaultValue: profile.AltEmail || "",
        })}
      </Card.Body>
      <Card.Footer>
        <Button className="btn-round mb-3" variant="success" type="submit">
          Update
        </Button>
      </Card.Footer>
    </form>
  );
};

const Profile = ({
  setNotification,
  profile,
  saveUserProfile,
  saveUserProfilePhoto,
  profileImage,
  deleteUserProfilePhoto,
  doChangePassword,
  profileSaveError,
  profilePhotoSaveError,
  profilePhotoDeleteError,
  passwordChangeError,
}) => {
  const onCrop = (blob) => {
    saveUserProfilePhoto(blob);
  };
  return (
    <div className="m-md-2 m-xl-4 m-1">
      <Container fluid>
        <Card>
          <Card.Body>
            <Container fluid>
              <Row>
                <Col md="3">
                  <Card className="bg-white">
                    <Card.Header>
                      <Card.Title>Profile Image </Card.Title>
                      <hr />
                    </Card.Header>
                    <Card.Body className="text-center">
                      {profilePhotoSaveError && (
                        <div className="text-danger pb-4">
                          {buildErrorMsg(profilePhotoSaveError)}
                        </div>
                      )}
                      {profilePhotoDeleteError && (
                        <div className="text-danger pb-4">
                          {buildErrorMsg(profilePhotoDeleteError)}
                        </div>
                      )}
                      <ImageUpload
                        minWidth={256}
                        minHeight={256}
                        aspect={1}
                        thumbnailClass="img-circle"
                        onCrop={onCrop}
                        initialImage={profileImage}
                        onRemove={deleteUserProfilePhoto}
                      />
                    </Card.Body>
                  </Card>
                </Col>
                <Col md="5">
                  <Card className="bg-white">
                    <Card.Header>
                      <Card.Title>Profile </Card.Title>
                      <hr />
                    </Card.Header>
                    <ProfileForm
                      doSave={saveUserProfile}
                      profile={profile}
                      actionError={profileSaveError}
                    />
                  </Card>
                </Col>
                <Col md="4">
                  <Card className="bg-white">
                    <Card.Header>
                      <Card.Title>Change Password</Card.Title>
                      <hr />
                    </Card.Header>
                    <ChangePasswordForm
                      doSave={doChangePassword}
                      actionError={passwordChangeError}
                    />
                  </Card>
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Container>
    </div>
  );
};
Profile.propTypes = {
  setNotification: PropTypes.func.isRequired,
  saveUserProfile: PropTypes.func.isRequired,
  saveUserProfilePhoto: PropTypes.func.isRequired,
  deleteUserProfilePhoto: PropTypes.func.isRequired,
  doChangePassword: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  activateEmailState: state.auth.activateEmailState,
  profile: state.user.profile,
  profileImage: state.user.photo,

  profileSaveError: state.user.profileSaveError,
  profilePhotoSaveError: state.user.profilePhotoSaveError,
  profilePhotoDeleteError: state.user.profilePhotoDeleteError,
  passwordChangeError: state.user.passwordChangeError,
});

export default connect(mapStateToProps, {
  setNotification,
  saveUserProfile,
  saveUserProfilePhoto,
  deleteUserProfilePhoto,
  doChangePassword,
})(Profile);
