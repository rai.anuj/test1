import React from "react";
import { Link } from "react-router-dom";

import { Button, Card, Image, Col, Row, Figure } from "react-bootstrap";
import YouTube from "react-youtube";

import ExampleImg from "../../assets/images/example-logo.png";

const View = () => {
  return (
    <div className="m-md-2 m-xl-4 m-1">
      <Card>
        <Card.Header>
          <Card.Title>Video #5</Card.Title>
          <hr />
        </Card.Header>
        <Card.Body>
          <Row>
            <Col xs={12} className="p-4">
              <div style={{ margin: "auto",maxWidth: "900px" }}>
                <YouTube
                  videoId="EngW7tLk6R8"
                  containerClassName={"youtubeContainer"}
                />
                <div className="text-center text-default">
                  Sample Video/Dummy Video
                </div>
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
};

export default View;
