import React, { useState } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

const List = ({ purchase }) => {
  return (
    <Jumbotron>
      <Row>
        <Col xs="12" sm="4" md="2" className="p-2">
          <Image alt="Example Channel" src={purchase.img} fluid />
        </Col>
        <Col xs="12" sm="8" md="10" className="p-2">
          <h1>{purchase.name}!</h1>
          <p>
            There are many variations of passages of Lorem Ipsum available, but
            the majority have suffered alteration in some form,
          </p>
          <div>
            <div className="social">
              <button className="btn btn-icon btn-round btn-twitter  mr-2">
                <FontAwesomeIcon icon={["fab", "twitter"]} />
              </button>
              <button className="btn btn-icon btn-round btn-instagram  mr-2">
                <FontAwesomeIcon icon={["fab", "instagram"]} />
              </button>
              <button className="btn btn-icon btn-round btn-facebook">
                <FontAwesomeIcon icon={["fab", "facebook-f"]} />
              </button>
            </div>
          </div>
        </Col>
      </Row>

      {purchase.collections &&
        purchase.collections.map((c) => (
          <Card className="mb-4 mt-4">
            <Card.Header>
              <Card.Title>
                {c.name}
                <hr />
              </Card.Title>
            </Card.Header>
            <Card.Body>
              <Row>
                {c.videos.map((v) => (
                  <Col md="4" lg="3" sm="6" xs="12">
                    <Link to={`/video/${v.id}`} className="text-info">
                      <Card>
                        <Card.Body>
                          <Image alt="Example Channel" src={v.coverImg} fluid />
                        </Card.Body>
                        <Card.Footer>{v.name}</Card.Footer>
                      </Card>
                    </Link>
                  </Col>
                ))}
              </Row>
            </Card.Body>
          </Card>
        ))}
    </Jumbotron>
  );
};

List.propTypes = {};

const mapStateToProps = (state) => ({});

export default withRouter(connect(mapStateToProps, {})(List));
