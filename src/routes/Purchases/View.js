import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { getPurchasedList } from "../../store/actions/purchase";

import PurchaseList from "./List";
import ExampleImg from "../../assets/images/example-logo.png";

const Index = (props) => {
  useEffect(() => {
    props.getPurchasedList();
  }, []);
  const purchase = props.purchase.list.filter((p) => p.id == props.match.params.id);
  return (
    <div className="m-md-1 m-xl-4 m-1">
      {purchase.length == 0
        ? "No matching channel found"
        : purchase.map((p) => <PurchaseList purchase={p} />)}
    </div>
  );
};

Index.propTypes = {
  getPurchasedList: PropTypes.func.isRequired,
  purchase: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({ purchase: state.purchase });

export default withRouter(
  connect(mapStateToProps, { getPurchasedList })(Index)
);
