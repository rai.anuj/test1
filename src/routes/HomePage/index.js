import React from "react";
import { Link } from "react-router-dom";

import { Button, Card, Image, Col, Row, Figure } from "react-bootstrap";
import ExampleImg from "../../assets/images/example-logo.png";
import MyChannels from "../MyChannels/MyChannels";

const HomePage = () => {
  return (
    <div className="m-md-2 m-xl-4 m-1">
      <MyChannels />
      <Card>
        <Card.Header>
          <Card.Title>My Purchases</Card.Title>
          <hr />
        </Card.Header>
        <Card.Body>
          <Row>
            <Col xs={6} md={3} className="pb-4">
              <Link to="/purchases/channel-35">
                <Figure>
                  <Figure.Image alt="Example Channel" src={ExampleImg} />
                  <Figure.Caption className="text-center">
                    <Card.Title className="text-center m-1" as="h6">
                      Channel 35
                    </Card.Title>
                    <Card.Text>Collection 2</Card.Text>
                  </Figure.Caption>
                </Figure>
              </Link>
            </Col>
            <Col xs={6} md={3} className="pb-4">
              <Link to="/purchases/channel-35">
                <Figure>
                  <Figure.Image alt="Example Channel" src={ExampleImg} />
                  <Figure.Caption className="text-center">
                    <Card.Title className="text-center text-center m-1" as="h6">
                      Channel 35
                    </Card.Title>
                    <Card.Text>Collection 3</Card.Text>
                  </Figure.Caption>
                </Figure>
              </Link>
            </Col>
            <Col xs={6} md={3} className="pb-4">
              <Link to="/purchases/channel-37">
                <Figure>
                  <Figure.Image alt="Example Channel" src={ExampleImg} />
                  <Figure.Caption className="text-center">
                    <Card.Title className="text-center text-center m-1" as="h6">
                      Channel 37
                    </Card.Title>
                    <Card.Text>Collection 3</Card.Text>
                  </Figure.Caption>
                </Figure>
              </Link>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
};

export default HomePage;
