import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import ChannelBox from "../../components/Channel/ChannelBox";
import { getAccessibleChannels } from "../../store/actions/channels";
import { IMAGE_BASE_URL } from "../../config";

import ExampleImg from "../../assets/images/example-logo2.jpg";

const Channels = ({ channelList, getAccessibleChannels }) => {
  useEffect(() => {
    getAccessibleChannels();
  }, []);

  const getLogo = (c) => {
    if (Array.isArray(c.images)) {
      let logo = c.images.filter((i) => i.fileuseidentifier == "logo");
      if (logo && logo.length > 0) {
        return IMAGE_BASE_URL + logo[0].filename;
      }
    }
    return false;
  };
  return (
    <div className="m-md-2 m-xl-4 m-1">
      <Card className="mb-4 figure-fluid">
        <Card.Header>
          <Card.Title>Channels</Card.Title>
          <hr />
        </Card.Header>
        <Card.Body>
          <Row>
            {channelList.map((v) => (
              <ChannelBox
                key={v.id}
                url={`/channels/${v.id}`}
                logo={getLogo(v)}
                title={v.orgname}
              />
            ))}
          </Row>
        </Card.Body>
      </Card>
    </div>
  );
};

Channels.propTypes = {
  getAccessibleChannels: PropTypes.func.isRequired,
  channelList: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  channelList: state.channels.list,
});

export default withRouter(
  connect(mapStateToProps, { getAccessibleChannels })(Channels)
);
