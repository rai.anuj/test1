import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import { addToCart } from "../../store/actions/purchase";
import { getAccessibleChannels } from "../../store/actions/channels";
import { getAccessibleCollections } from "../../store/actions/collections";

import { getChannelImage, getSocial } from "../../utils/images";
import VideoBox from "../../components/Channel/VideoBox";

import "./index.css";

const Channel = (props) => {
  useEffect(() => {
    if (props.channelList.length == 0) {
      props.getAccessibleChannels();
    }
    if (!Array.isArray(props.collections[props.match.params.id])) {
      props.getAccessibleCollections(props.match.params.id);
    }
  }, []);

  const channel = props.channelList.find((c) => c.id == props.match.params.id);
  let bg;
  let logo;
  if (channel) {
    bg = getChannelImage(channel.images, "banner");
    logo = getChannelImage(channel.images, "logo");
  }
  const collections = props.collections[props.match.params.id];
  return (
    <div className="m-md-1 m-xl-4 m-1">
      {channel ? (
        <React.Fragment>
          <div>
            {bg && (
              <div
                className="channel-banner-img"
                style={{ backgroundImage: `url(${bg})` }}
              ></div>
            )}
            <Jumbotron style={{ padding: "1rem 1rem 0.5rem 1rem" }}>
              <div>
                {logo && (
                  <div className="channel-page-logo-container">
                    <Image fluid src={logo} />
                  </div>
                )}
                <div
                  className={`channel-page-detail-container ${
                    logo && "pl-0 pl-lg-4"
                  }`}
                >
                  <div className="name">{channel.orgname}</div>
                  <div className="address">{channel.address}</div>
                  <p className="pt-2 text-default">{channel.descr}</p>
                </div>
              </div>
              <hr style={{ marginTop: "0.5rem", marginBottom: "0.5rem" }} />
              <div className="clearfix">
                <div className="social float-md-left">{getSocial(channel)}</div>
                <div className="float-md-right pt-1">
                  <small>
                    <span className="badge badge-info mr-2 d-none d-md-inline-block">
                      <i className="nc-icon nc-single-02"></i>
                    </span>
                    <span className="d-block d-md-inline">
                      <b>Membership Type: </b>Annual
                    </span>
                    <span
                      className="d-none d-md-inline-block"
                      style={{ color: "#b4b1ae", padding: "0px 10px" }}
                    >
                      |
                    </span>
                    <span className="d-block d-md-inline">
                      <b>Membership Status: </b>Active
                    </span>
                  </small>
                </div>
              </div>
            </Jumbotron>
          </div>
          {collections &&
            collections.map((c) => (
              <Card key={c.id}>
                <Card.Header className="clearfix pr-3 pl-3">
                  {getChannelImage(c.collectionimages, "cover") && (
                    <div className="channel-page-logo-container">
                      <Image
                        fluid
                        src={getChannelImage(c.collectionimages, "cover")}
                      />
                    </div>
                  )}
                  <div
                    className={`channel-page-detail-container ${
                      getChannelImage(c.collectionimages, "cover") &&
                      "pl-0 pl-lg-4"
                    }`}
                  >
                    <div className="clearfix">
                      <Card.Title className="float-left">
                        {c.collname}
                      </Card.Title>
                      <Button
                        className="float-right"
                        size="sm"
                        variant="danger"
                        onClick={props.addToCart}
                      >
                        <i className="fa fa-shopping-cart" /> Add to cart
                      </Button>
                    </div>
                    <Card.Text>{c.descr}</Card.Text>
                  </div>
                  <hr />
                </Card.Header>
                <Card.Body>
                  <Row className="figure-fluid">
                    {c.collectionvideo.map((v, k) => (
                      <Col
                        xs={12}
                        sm={6}
                        md={4}
                        lg={3}
                        className="p-4 cursor-pointer"
                        onClick={() => console.log(true)}
                        key={k}
                      >
                        <VideoBox video={v} showCart />
                      </Col>
                    ))}
                  </Row>
                </Card.Body>
              </Card>
            ))}
        </React.Fragment>
      ) : (
        "Channel not found"
      )}
    </div>
  );
};

Channel.propTypes = {
  addToCart: PropTypes.func.isRequired,
  purchase: PropTypes.object.isRequired,
  collections: PropTypes.object.isRequired,
  getAccessibleChannels: PropTypes.func.isRequired,
  channelList: PropTypes.array.isRequired,
  getAccessibleCollections: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  purchase: state.purchase,
  channelList: state.channels.list,
  collections: state.collections.collections,
});

export default withRouter(
  connect(mapStateToProps, {
    addToCart,
    getAccessibleChannels,
    getAccessibleCollections,
  })(Channel)
);
