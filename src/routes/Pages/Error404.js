import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Redirect } from "react-router-dom";

const Error404 = (props) => {
  return (
    <Redirect to="/" />
  );
};

export default Error404;