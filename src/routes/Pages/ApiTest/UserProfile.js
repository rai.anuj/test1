import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";
import ReactCrop from 'react-image-crop';
import "react-image-crop/dist/ReactCrop.css";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Redirect } from "react-router-dom";
import {
  getUserProfile,
  saveUserProfile,
  getUserProfilePhoto,
} from "../../../store/actions/user";

const ApiTest = ({
  getUserProfile,
  profile,
  saveUserProfile,
  getUserProfilePhoto,
  photo,
}) => {
  useEffect(() => {
    //getUserProfile();
    // saveUserProfile({
    //   FirstName: "Anuj",
    //   LastName: "Rai",
    //   Id: "100100100100",
    //   AltEmail: null,
    //   Phone: null,
    // });
    getUserProfilePhoto();
  }, []);
  console.log(profile, photo);
  return <Card>Api Test</Card>;
};

ApiTest.propTypes = {
  getUserProfile: PropTypes.func.isRequired,
  saveUserProfile: PropTypes.func.isRequired,
  getUserProfilePhoto: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  photo: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.user.profile,
  photo: state.user.photo,
});

export default connect(mapStateToProps, {
  getUserProfile,
  saveUserProfile,
  getUserProfilePhoto,
})(ApiTest);
