import React, { useEffect } from "react";
import {
  Button,
  Jumbotron,
  Tabs,
  Tab,
  Card,
  Row,
  Col,
  Figure,
  Image,
} from "react-bootstrap";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Redirect } from "react-router-dom";
import {
  createChannel,
  cancelChannelAccount,
} from "../../../store/actions/channels";
const ApiTest = ({ createChannel, cancelChannelAccount }) => {
  useEffect(() => {
    //profile
    cancelChannelAccount("46214a21-19e0-4da4-b0bf-63029785f161");
    // createChannel({
    //   OrgName: "Test One",
    //   Descr: "Test Channel",
    //   Address: "Burr Ridge, IL, USA",
    //   ServiceLocations: "USA",
    //   Phone: "630-888-0987",
    //   Email: "info@icarnatic.org",
    //   Website: "www.maximum.in",
    //   Facebook: "fb",
    //   YouTube: null,
    //   LinkedIn: "",
    //   Instagram: "",
    //   Twitter: null,
    //   WhatsApp: null,
    //   HideToNonMembers: false,
    //   IsCreator: true,
    //   IsArtist: false,
    //   IsSabha: true,
    //   Id: "a1a91210-983d-409e-8896-d0086e3bb1dd",
    //   CreateBy: "ravi@icarnatic.org",
    //   CreateDttm: "2020-09-23T17:12:21",
    //   LastUpdBy: "ravi@icarnatic.org",
    //   LastUpdDttm: "2020-09-23T17:29:15",
    // });
    // createChannel({
    //   OrgName: "Test Channel",
    //   Website: "www.example.com ",
    //   CountryCode: "USA",
    //   Address: "Devon Ridge Dr",
    //   City: "Burr Ridge",
    //   State: "IL",
    //   Postal: "60555",
    //   PhoneCountryCode: "+1",
    //   Phone: "(630) 222-5678",
    //   PrimaryContact: "Manimaran VK",
    //   PrimaryEmail: "example@example.com",
    //   PrimaryPhoneCountryCode: "+1",
    //   PrimaryPhone: "(630) 786-6630",
    //   PrimaryMobileCountryCode: "",
    //   PrimaryMobile: "",
    //   BillingContact: "Test User",
    //   BillingEmail: "example@example.com",
    //   BillingPhoneCountryCode: "",
    //   BillingPhone: "",
    //   BillingMobileCountryCode: "",
    //   BillingMobile: "",
    //   SubscriptionKey: "",
    //   SubscriptionStatus: 1,
    // });
  }, []);
  return <Card>Api Test</Card>;
};

ApiTest.propTypes = {
  createChannel: PropTypes.func.isRequired,
  cancelChannelAccount: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  createChannel,
  cancelChannelAccount,
})(ApiTest);
