import React from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import Base from "../Auth/Base";

class ResetEmailSent extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/" />;
    }
    return (
      <Base>
        <Card
          className="card-register text-center"
          style={{ backgroundColor: "#FFF" }}
        >
          <Card.Header>
            <Card.Title tag="h4">Email sent!</Card.Title>
          </Card.Header>
          <Card.Body>
            <p>
              We have sent an email with a password reset link to your email
              address. In order to complete the reset password process, please
              click the password reset link in the email.
            </p>
            <p>
              If you do not receive a password reset email, please check your
              spam folder. Also, please verify that you entered a valid email
              address in our password reset form.
            </p>
          </Card.Body>
          <Card.Footer className="p-4">
            <Button variant="info" as={Link} to="/">
              Back to home
            </Button>
          </Card.Footer>
        </Card>
      </Base>
    );
  }
}

ResetEmailSent.propTypes = {
  isAuth: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
});

export default connect(mapStateToProps, {})(ResetEmailSent);
