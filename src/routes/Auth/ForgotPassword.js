import React from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";

import { useForm } from "react-hook-form";

import { inputGroupPrepend, checkBox } from "../../components/Form/Input";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { doReset } from "../../store/actions/auth";
import Base from "./Base";

const ResetForm = ({ doReset, resetError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    doReset(data.email);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card.Body>
        {resetError && (
          <div className="text-danger pb-4">
            <b>The email provided is incorrect.</b>
          </div>
        )}
        {inputGroupPrepend({
          name: "email",
          type: "email",
          placeHolder: "Email",
          validation: {
            required: true,
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Please provide valid email",
            },
          },
          icon: "nc-icon nc-email-85",
          defaultError: "Please provide valid email",
          register,
          errors,
        })}
        <br />
      </Card.Body>
      <Card.Footer>
        <Button
          block
          className="btn-round mb-3"
          variant="success"
          type="submit"
        >
          Reset
        </Button>
        <div className="text-center">
          <Link to="/login">Login</Link>
        </div>
        <div className="text-center">
          <Link to="/register">Signup!</Link>
        </div>
      </Card.Footer>
    </form>
  );
};

class Login extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/" />;
    }
    if (this.props.resetEmailSent) {
      return <Redirect to="/reset-email-sent" />;
    }
    return (
      <Base>
        <Card className="text-center" style={{ backgroundColor: "#FFF" }}>
          <Card.Header>
            <Card.Title tag="h4">Reset Password</Card.Title>
          </Card.Header>
          <ResetForm
            doReset={this.props.doReset}
            resetError={this.props.resetError}
          />
        </Card>
      </Base>
    );
  }
}

Login.propTypes = {
  doReset: PropTypes.func.isRequired,
  isAuth: PropTypes.bool.isRequired,
  resetError: PropTypes.bool.isRequired,
  resetEmailSent: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  resetError: state.auth.resetError,
  resetEmailSent: state.auth.resetEmailSent,
});

export default connect(mapStateToProps, { doReset })(Login);
