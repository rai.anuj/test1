import React from "react";

import { Card, Container, Col, Row } from "react-bootstrap";

class Base extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    return (
      <div className="login-page full-page ">
        <Container fluid style={{ zIndex: "2", position: "relative" }}>
          <Row>
            <Col className="ml-auto mr-auto p-4" lg="4" md="6">
              {this.props.children}
            </Col>
          </Row>
        </Container>
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(/bg1.jpg)`,
          }}
        ></div>
      </div>
    );
  }
}

export default Base;
