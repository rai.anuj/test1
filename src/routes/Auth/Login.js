import React from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import { inputGroupPrepend, checkBox } from "../../components/Form/Input";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { doLogin } from "../../store/actions/auth";
import Base from "./Base";

const LoginForm = ({ doLogin, loginError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    doLogin({
      email: data.email,
      password: data.password,
    });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card.Body>
        {loginError && (
          <div className="text-danger pb-4">
            <b>The user name or password is incorrect.</b>
          </div>
        )}
        {inputGroupPrepend({
          name: "email",
          type: "email",
          placeHolder: "Email",
          validation: {
            required: true,
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Please provide valid email",
            },
          },
          icon: "nc-icon nc-email-85",
          defaultError: "Please provide valid email",
          register,
          errors,
        })}
        {inputGroupPrepend({
          name: "password",
          type: "password",
          placeHolder: "Password",
          validation: {
            required: { value: true, message: "Password is required" },
          },
          icon: "nc-icon nc-key-25",
          register,
          errors,
        })}
        <br />
      </Card.Body>
      <Card.Footer>
        <Button
          block
          className="btn-round mb-3"
          variant="success"
          type="submit"
        >
          Login
        </Button>
        <div className="text-center">
          <Link to="/forgot-password">Forgot Password?</Link>
        </div>
        <div className="text-center">
          <Link to="/register">Signup!</Link>
        </div>
      </Card.Footer>
    </form>
  );
};

class Login extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/" />;
    }
    return (
      <Base>
        <Card  style={{ backgroundColor: "#FFF" }}>
          <Card.Header>
            <h3 className="header text-center">Login</h3>
          </Card.Header>
          <LoginForm
            doLogin={this.props.doLogin}
            loginError={this.props.loginError}
          />
        </Card>
      </Base>
    );
  }
}

Login.propTypes = {
  doLogin: PropTypes.func.isRequired,
  isAuth: PropTypes.bool.isRequired,
  loginError: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  loginError: state.auth.loginError,
});

export default connect(mapStateToProps, { doLogin })(Login);
