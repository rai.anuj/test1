import React from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import Base from "./Base";

import PropTypes from "prop-types";
import { connect } from "react-redux";

const ResetPassword = ({}) => {
  return <Base>Reset Password</Base>;
};

ResetPassword.propTypes = {};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {})(ResetPassword);
