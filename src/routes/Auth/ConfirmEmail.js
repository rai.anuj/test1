import React, { useEffect } from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";

import PropTypes from "prop-types";
import { connect } from "react-redux";

import Base from "./Base";
import { doActivateEmail } from "../../store/actions/auth";
import { setNotification } from "../../store/actions/misc";
const ConfirmEmail = ({
  doActivateEmail,
  activateEmailState,
  setNotification,
}) => {
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const userId = urlParams.get("userId");
    const code = urlParams.get("code");
    doActivateEmail({ userId, code });
  }, []);

  useEffect(() => {
    if (activateEmailState == "DONE") {
      const msg = <span>Your account was activated. You can now login.</span>;
      setNotification({ message: msg });
    } else if (activateEmailState == "FAILED") {
      const msg = (
        <span>
          Your account activation failed. Please click on the link sent in the
          email again
        </span>
      );
      setNotification({ message: msg });
    }
  }, [activateEmailState]);
  if (activateEmailState == "DONE") {
    return <Redirect to="/login" />;
  } else if (activateEmailState == "FAILED") {
    return <Redirect to="/login" />;
  }
  return (
    <Base>
      <Card className="text-center" style={{ backgroundColor: "#FFF" }}>
        <Card.Body>Please wait... Activationg your account.</Card.Body>

        <Card.Footer className="p-4">
          <Button variant="info" as={Link} to="/">
            Back to home
          </Button>
        </Card.Footer>
      </Card>
    </Base>
  );
};

ConfirmEmail.propTypes = {
  doActivateEmail: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  activateEmailState: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  activateEmailState: state.auth.activateEmailState,
});

export default connect(mapStateToProps, { doActivateEmail, setNotification })(
  ConfirmEmail
);
