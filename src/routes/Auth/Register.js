import React from "react";
import { Redirect, Link } from "react-router-dom";

import {
  Button,
  Card,
  Label,
  Form,
  InputGroup,
  Container,
  Col,
  Row,
} from "react-bootstrap";
import { useForm } from "react-hook-form";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { doRegister } from "../../store/actions/auth";
import { inputGroupPrepend, checkBox } from "../../components/Form/Input";
import Base from "./Base";

const RegisterationForm = ({ doRegister, registerationError }) => {
  const { register, handleSubmit, errors, watch } = useForm(); // initialize the hook
  const onSubmit = (data) => {
    doRegister({
      Name: data.Name,
      Email: data.Email,
      Password: data.Password,
      ConfirmPassword: data.ConfirmPassword,
    });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Card.Body>
        {registerationError && (
          <div className="text-danger pb-4 text-left">
            {Array.isArray(registerationError) ? (
              <ul>
                {registerationError.map((e) => (
                  <li>{e}</li>
                ))}
              </ul>
            ) : (
              <b>Something went wrong</b>
            )}
          </div>
        )}
        {inputGroupPrepend({
          name: "Name",
          type: "text",
          placeHolder: "Full Name",
          validation: { required: true },
          icon: "nc-icon nc-circle-10",
          defaultError: "Please provide full name",
          register,
          errors,
        })}
        {inputGroupPrepend({
          name: "Email",
          type: "email",
          placeHolder: "Email",
          validation: {
            required: true,
            pattern: {
              value: /\S+@\S+\.\S+/,
              message: "Please provide valid email",
            },
          },
          icon: "nc-icon nc-email-85",
          defaultError: "Please provide valid email",
          register,
          errors,
        })}
        {inputGroupPrepend({
          name: "Password",
          type: "password",
          placeHolder: "Password",
          validation: {
            required: { value: true, message: "Password is required" },
            pattern: {
              value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,30}$/,
              message:
                "Password must me atleast 6 characters, at least one uppercase letter, one lowercase letter, one number and one special character(@$!%)",
            },
          },
          icon: "nc-icon nc-key-25",
          register,
          errors,
        })}
        {inputGroupPrepend({
          name: "ConfirmPassword",
          type: "password",
          placeHolder: "Confirm Password",
          validation: {
            required: { value: true, message: "Confirm Password is required" },
            minLength: {
              value: 6,
              message: "Confirm Password minimum length is 6",
            },
            validate: (value) =>
              value === watch("Password") || "Confirm password doesn't match",
          },
          icon: "nc-icon nc-key-25",
          register,
          errors,
        })}
        <br />
        {checkBox({
          name: "terms",
          label: (
            <React.Fragment>
              I agree to the{" "}
              <a href="#pablo" onClick={(e) => e.preventDefault()}>
                terms and conditions
              </a>
              .
            </React.Fragment>
          ),
          validation: {
            required: {
              value: true,
              message: "Please acept the terms and condition",
            },
          },
          register,
          errors,
          type: "checkbox",
        })}
      </Card.Body>
      <Card.Footer>
        <Button
          type="submit"
          block
          className="btn-round mb-3"
          variant="success"
        >
          Register
        </Button>
        <div className="text-center">
          <Link to="/login">Login</Link>
        </div>
      </Card.Footer>
    </form>
  );
};
class Register extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    if (this.props.isAuth) {
      return <Redirect to="/" />;
    }
    if (this.props.registerationDone) {
      return <Redirect to="/registeration-done" />;
    }
    return (
      <Base>
        <Card
          className="card-register text-center"
          style={{ backgroundColor: "#FFF" }}
        >
          <Card.Header>
            <Card.Title tag="h4">Register</Card.Title>
            <div className="social">
              <Button className="btn-icon btn-round" variant="twitter">
                <i className="fa fa-twitter" />
              </Button>
              <Button className="btn-icon btn-round" variant="dribbble">
                <i className="fa fa-dribbble" />
              </Button>
              <Button className="btn-icon btn-round" variant="facebook">
                <i className="fa fa-facebook-f" />
              </Button>
              <p className="card-description">or be classical</p>
            </div>
          </Card.Header>
          <RegisterationForm
            doRegister={this.props.doRegister}
            registerationError={this.props.registerationError}
          />
        </Card>
      </Base>
    );
  }
}

Register.propTypes = {
  doRegister: PropTypes.func.isRequired,
  isAuth: PropTypes.bool.isRequired,
  registerationDone: PropTypes.bool.isRequired,
  registerationError: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  registerationDone: state.auth.registerationDone,
  registerationError: state.auth.registerationError,
});

export default connect(mapStateToProps, { doRegister })(Register);
