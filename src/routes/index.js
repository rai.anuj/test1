import React from "react";
import Login from "./Auth/Login";
import Register from "./Auth/Register";
import ForgotPassword from "./Auth/ForgotPassword";

import HomePage from "./HomePage";
import AdminChannelList from "./Admin";

import AdminChannelEdit from "./Admin/Edit";
import CreateChannel from "./Admin/CreateChannel";
import Channel from "./Channels/ChannelView";
import Channels from "./Channels/List";

import MyChannels from "./MyChannels";
import Purchases from "./Purchases";
import PurchasesView from "./Purchases/View";
import VideoView from "./Video/View";
import Error404 from "./Pages/Error404";
import RegisterationDone from "./Pages/RegisterationDone";
import ConfirmEmail from "./Auth/ConfirmEmail";
import ResetPassword from "./Auth/ResetPassword";

import ResetEmailSent from "./Pages/ResetEmailSent";

import Profile from "./Profile";
import ApiTest from "./Pages/ApiTest";
const routes = [
  {
    path: "/",
    exact: true,
    name: "Home",
    icon: ["fas", "home"],
    main: () => <HomePage />,
    sidebar: true,
    base: "",
  },
  {
    path: "/channels",
    exact: true,
    name: "Channels",
    icon: ["fas", "broadcast-tower"],
    main: () => <Channels />,
    sidebar: true,
    base: "",
  },
  {
    path: "/channels/:id",
    exact: true,
    main: () => <Channel />,
    sidebar: false,
    base: "",
  },
  {
    path: "/my-channels",
    exact: true,
    name: "My Channels",
    icon: ["fas", "heart"],
    main: () => <MyChannels />,
    sidebar: true,
    base: "",
  },
  {
    path: "/purchases",
    exact: true,
    name: "Purchases",
    icon: ["fas", "shopping-basket"],
    main: () => <Purchases />,
    sidebar: true,
    base: "",
  },
  {
    path: "/purchases/:id",
    exact: true,
    main: () => <PurchasesView />,
    sidebar: false,
    base: "",
  },
  {
    path: "/video/:id",
    exact: true,
    main: () => <VideoView />,
    sidebar: false,
    base: "",
  },
  {
    path: "/login",
    exact: true,
    main: () => <Login />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/register",
    exact: true,
    main: () => <Register />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/registeration-done",
    exact: true,
    main: () => <RegisterationDone />,
    sidebar: false,
    base: "",
    isPublic: true,
  },

  {
    path: "/confirm-email",
    exact: true,
    main: () => <ConfirmEmail />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/forgot-password",
    exact: true,
    main: () => <ForgotPassword />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/reset-password",
    exact: true,
    main: () => <ResetPassword />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/reset-email-sent",
    exact: true,
    main: () => <ResetEmailSent />,
    sidebar: false,
    base: "",
    isPublic: true,
  },
  {
    path: "/admin/create-channel",
    exact: true,
    main: () => <CreateChannel />,
    sidebar: false,
    base: "",
  },
  {
    path: "/admin",
    exact: true,
    main: () => <AdminChannelList />,
    sidebar: false,
    base: "",
  },

  {
    path: "/admin/:id",
    exact: false,
    main: () => <AdminChannelEdit />,
    sidebar: false,
    base: "",
  },
  {
    path: "/profile",
    exact: true,
    main: () => <Profile />,
    sidebar: false,
    base: "",
  },
  {
    path: "/api-test",
    exact: true,
    main: () => <ApiTest />,
    sidebar: false,
    base: "",
  },
  {
    path: "/",
    exact: false,
    main: () => <Error404 />,
    sidebar: false,
    base: "",
  },
];
export default routes;
