var FtpDeploy = require("ftp-deploy");
var ftpDeploy = new FtpDeploy();
require("dotenv").config();

var config = {
  user: process.env.FTP_USER,
  // Password optional, prompted if none given
  password: process.env.FTP_PASS,
  host: process.env.FTP_HOST,
  port: 21,
  localRoot: __dirname + "/build",
  remoteRoot: process.env.FTP_PATH,
  include: ["*", "**/*"], // this would upload everything except dot files
  // include: ["*.php", "dist/*", ".*"],
  // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
  exclude: [
    "dist/**/*.map",
    "node_modules/**",
    "node_modules/**/.*",
    ".git/**",
  ],
  // delete ALL existing files at destination before uploading, if true
  deleteRemote: process.env.FTP_DELETE_EXISTING,
  // Passive mode is forced (EPSV command is not sent)
  forcePasv: true,
};

ftpDeploy
  .deploy(config)
  .then((res) => console.log("finished:", res))
  .catch((err) => console.log(err));
